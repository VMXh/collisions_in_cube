/*
 Версия 6 мая 2011 года.
 Программа считывает информацию из файлов:
	1. save_file.txt - считывание параметров объема, координат и скоростей частиц.
	2. program.txt - считывание алгоритма работы прораммы. Структура этого файла:
		1. К - число "шагов" алгоритма
		2. Последовательность К команд, каждая на новой строчке.
			Команды могут быть:
			save file.txt - сохранить параметры объема, координаты и скорости частиц в файл file.txt
			load_seed - загрузить систему из файла save_file.txt
			step 5000 - ожидать, пока в системе произойдет в среднем 5000 соударений на каждую частицу.
			image image_file.txt - сохранить в image_file.txt данные для построения графика профиля плотности.
			compress 0.451 - сжать систему до плотности 0.451

*/

#include <iostream>   // библиотека потоков ввода-вывода
#include <math.h>     // математика
//#include <math.hpp>
#include <stdlib.h>   // стандартная библиотека
#include <stdio.h>    // для вывода в файлы
//#include <time.h>     // для подсчета времени выполнения
//#include <omp.h>
#include <algorithm>
#include <vector>
#include <fstream>
using namespace std;

#define N_MAX 7100


  //изм_Гераськин
  int Num0[200];
  int it0;
  int Num1[200];
  int it1;
  int Num2[200];
  int it2;
  int Num3[200];
  int it3;
  int Num4[200];
  int it4;
  int Num5[200];
  int it5;
  int Num6[200];
  int it6;
  int Num7[200];
  int it7;
  bool Num0_m[200];
  bool Num1_m[200];
  bool Num2_m[200];
  bool Num3_m[200];
  bool Num4_m[200];
  bool Num5_m[200];
  bool Num6_m[200];
  bool Num7_m[200];


int N=0;		// Количество молекул
double A, L;	// Параметры объема. А - максимум по у и z, L - максимум по х.
const double PI = 3.14159265358979;

int CR; // подсчет количества соударений в выбранном объеме.
double Npress; // число сфер в объеме, рассматриваемом при измерении давления.
double Time; // время измерения давления, в единицах времени системы.
bool STOP = false; // если в системе произошло необходимое число оударений, то остановиться.

int x1 = 0.0;
int x2 = 0.0;

// структура координат частиц.
// координата Х для частицы и ее образов одинакова.
// координаты Y и Z для частицы и ее оразов различны.
// NOBR - количество образов, которое имеет частица.
// OBR - номер образа, если он единственный (1 - по Y, 2 - по Z)
// p - координата, где должна отразиться частица на границе, противоположной идеальной стенке.
struct coord {
	double x;
	double y[4];
	double z[4];
	double vx,vy,vz;

	int NOBR;
	int OBR;

	double p;
};
struct coord coor[N_MAX];	// структура кооржинат

double cube;         // расстояние, которое определяет куб "соседей" для рассчета столкновений.

FILE *error_file;         	// файл для вывода ошибок
FILE *impulse_file;			// файл для вывода импульса и момента импульса системы

FILE *image_file;	// файл для вывода данных по профилю плотности.
FILE *save_file; 	// анения параметров системы.

FILE *image_profile_file;	// файл для сохранения разреза пиков.

////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////

void Del_mas(int nm_m)
{
     if ( coor[nm_m].NOBR > 0 )
       {
           for(int i0=0;i0<it0;i0++) if (Num0[i0]==nm_m) {Num0_m[i0]=false;break;}
           for(int i1=0;i1<it1;i1++) if (Num1[i1]==nm_m) {Num1_m[i1]=false;break;}
           for(int i2=0;i2<it2;i2++) if (Num2[i2]==nm_m) {Num2_m[i2]=false;break;}
           for(int i3=0;i3<it3;i3++) if (Num3[i3]==nm_m) {Num3_m[i3]=false;break;}
           for(int i4=0;i4<it4;i4++) if (Num4[i4]==nm_m) {Num4_m[i4]=false;break;}
           for(int i5=0;i5<it5;i5++) if (Num5[i5]==nm_m) {Num5_m[i5]=false;break;}
           for(int i6=0;i6<it6;i6++) if (Num6[i6]==nm_m) {Num6_m[i6]=false;break;}
           for(int i7=0;i7<it7;i7++) if (Num7[i7]==nm_m) {Num7_m[i7]=false;break;}
       }
}




/*
данная функция используется в компиляторах,
не поддерживающих эту функцию.
*/
int Random(int r)
{
	return rand() % r;
}


/*
// процедура нового посева системы.
// получает в качестве параметров
// NN - число частиц в ребре
// etta - начальная плотность
// Srkv - средняя квадратичная скорость.
void new_seed( int NN, double etta, double Srkv ) {
	int KZ = 2;

	double axy,axz,v0x,v0y,v0z,v1x,v1y,v1z,v2x,v2y,v2z,v3x,v3y,v3z;

	double rb = 2.0 * sqrtl(2.0);      // расстояние между двумя слоями
	double rbp = sqrtl(2.0);
	double A0 = rb*(NN - 1.0) + 2.0;         // рассчет параметров начального объема
	double L0 = rb*(2.0 * NN - 1.0) + 2.0;        //

	double mL0 = L0;

	double Betta = 0.0;

    double XC, YC, ZC; // координаты центра объема
    int NA;      // количество посеянных частиц
    double ax, ay, az, vx, vy, vz;

    N = 2.0*NN*( NN*NN + (NN - 1.0) * (NN - 1.0) );   //  ожидаемое число частиц.
    N = N + (2.0 * NN - 1.0) * 2.0 * (NN - 1.0) * NN;         //

    double etta0 = (4.0 * M_PI * N)/(3.0 * A0 * A0 * (L0-1) );   // рассчет первоначальной плотности.

    double Alpha = etta0 / etta;

    double sk = ( 1.0 / (Alpha*(L0-1.0) ) ) * sqrtl( (L0 * L0 / 4.0) + 1.0 / (27.0L * Alpha * (L0-1.0) ) );

    Betta = expl( (1.0 / 3.0) * logl( L0 / (2.0 * Alpha * (L0-1.0)) + sk) );
    Betta = Betta + expl( (1.0 / 3.0) * logl( -L0 / (2.0 * Alpha * (L0-1.0)) - sk) );

    Betta = 1.0 / Betta;

    XC = L0 / 2.0;    //
    YC = A0 / 2.0;    //  вычисление центра объема.
    ZC = A0 / 2.0;    //

    L0 = L0*KZ;
    L = L0 * Betta;
    A = A0 * Betta;

    NA = 0;

    for (int i = 0; i < 2*NN; i++)
        for (int j = 0; j < NN; j++)
            for (int k = 0; k < NN/2; k++) {
                vx = RandG(0.0, Srkv);  //  задаем скорость.
                vy = RandG(0.0, Srkv);  //  она будет одинакова для нескольких частиц сразу(с небольшим отклонением)
                vz = RandG(0.0, Srkv);  //  чтобы был равен нулю импульс и момент импульса системы.

                for (int ii = -1; ii < 2; ii += 2) {
                    if ((i==0) && (ii>0)) continue;

                    ax = XC + i*ii*rbp;

                    for (int jj = -1; jj < 2; jj += 2) {
                        if ((j==0) && (jj>0)) continue;

                        ay = YC + j*jj*rbp;

                        for (int kk = -1; kk < 2; kk += 2) {
                            if ( ( (i%2 == 0) && (j%2 == 0) ) || ( (i%2 != 0) && (j%2 != 0) ) )
                                az = ZC + kk*(rbp + k*rb);
                            else {
                                if ((k==0) && (kk > 0)) continue;
                                az = ZC + kk*k*rb;
                            }

                            coor[NA].x = ax;     	// назначаем координаты для новой молекулы
                            coor[NA].y[0] = ay;     // множитель Betta - это коэффициент расширения
                            coor[NA].z[0] = az;     // на него умножаются все координаты и параметры объема

							if ( (j==0) && (fabs(ZC-az)<0.1E-15L) && ( (i/2)*2!=i) )
								if ( ((i+1)/4)*4 != (i+1) ) {
									axy = vy;
									axz = vz;
									if (ii>0) { vy=-vy; vz=-vz; }
									vy = ii*vy;
									vz = ii*vz;
								} else {
									vy = axy;
									vz = axz;
									vy = ii*vy;
									vz = ii*vz;
								}
							if (i==0) {
								if ((j==0) && (kk<0))
									switch (k) {
										case 0: v0x=vx; v0y=vy; v0z=vz; break;
										case 1: v1x=vx; v1y=vy; v1z=vz; break;
										case 2: v2x=vx; v2y=vy; v2z=vz; break;
										case 3: v3x=vx; v3y=vy; v3z=vz; break;
									}
								else
									switch (k) {
										case 0: vx=-v0x; vy=-v0y; vz=-v0z; break;
										case 1: vx=-v1x; vy=-v1y; vz=-v1z; break;
										case 2: vx=-v2x; vy=-v2y; vz=-v2z; break;
										case 3: vx=-v3x; vy=-v3y; vz=-v3z; break;
									}

								if ( (k==0) && ( (j/2)*2!=j ) )
									switch (j) {
										case 1: vx=jj*v0x; vy=jj*v0y; vz=jj*v0z; break;
										case 3: vx=jj*v1x; vy=jj*v1y; vz=jj*v1z; break;
										case 5: vx=jj*v2x; vy=jj*v2y; vz=jj*v2z; break;
										case 7: vx=jj*v3x; vy=jj*v3y; vz=jj*v3z; break;
									}
							}

                            coor[NA].vx = vx*ii*jj*kk;
                            coor[NA].vy = vy*ii*jj*kk;
                            coor[NA].vz = vz*ii*jj*kk;

                            NA++;
                        }
                    }
                }
            }

	// копируем систему столько раз, сколько потребуется.
    for (int jj = 1; jj < KZ; jj++) {
        for (int ii = 0; ii < N; ii++) {
            coor[ii+jj*N].x = coor[ii].x + jj*mL0;
            coor[ii+jj*N].y[0] = coor[ii].y[0];
            coor[ii+jj*N].z[0] = coor[ii].z[0];

            coor[ii+jj*N].vx = coor[ii].vx;
            coor[ii+jj*N].vy = coor[ii].vy;
            coor[ii+jj*N].vz = coor[ii].vz;
        }
    }

    N = N*KZ;

	// "расширяем" систему до необходимой плотности.
    for (int ii = 0; ii < N; ii++) {
        coor[ii].x = coor[ii].x*Betta;
        coor[ii].y[0] = coor[ii].y[0]*Betta;
        coor[ii].z[0] = coor[ii].z[0]*Betta;
    }

	// сообщаем каждой сфере, где она должна отражаться.
    for (int ii = 0; ii < N; ii++)
		coor[ii].p = L - Random(9)*0.1L;

	/// задаем начальные значения для координат образов.
	for (int i = 0; i < N_MAX; i++) {
		coor[i].y[1] = -10.0;
		coor[i].y[2] = -10.0;
		coor[i].y[3] = -10.0;




		coor[i].z[1] = -10.0;
		coor[i].z[2] = -10.0;
		coor[i].z[3] = -10.0;

		coor[i].NOBR = 0;
	}


	// рассчитываем ребро куба взаимодействий.
    cube = expl( (1.0 / 3.0) * logl( 2.0 * M_PI / etta ) );   // половина ребра куба,
                                                        // в котором вычисляются столкновения
	cube = cube + 2.0;

	// выводим параметры системы после посева.
	std::cout << " \n cube=" << cube << ";\n";
	std::cout << " N=" << N << ";  A=" << A << ";  L=" << L << ";\n";
	std::cout << " etta=" << (4.0 * M_PI * N) / (3.0 * A * A * (L - 1.0) ) << ";\n";
}
*/

// процедура загрузки координат и скоростей частиц из файла.
// NN - всегда только четное число(для правильной работы алгоритма)
// это число "слоев" по у и z
// etta - это задаваемая плотность
// Srkv - среднеквадратичное отклонение для распределения по скоростям.
void load_seed() {
	double a1,a2,a3,a4;
	int i;

	// считываем параметры ячейки
	FILE *loading_file = fopen("save_file.txt", "r");
	fscanf(loading_file, "%i\n", &N);
	fscanf(loading_file, "%le\n", &a1);
	A = a1;
	fscanf(loading_file, "%le\n", &a1);
	L = a1;
	printf("N=%i  A=%f  L=%f \n", N, A, L);

	// считываем параметры частиц, создаем образы
	for (i = 0; i < N; i++)
    {
		fscanf(loading_file, "%le %le %le %le\n", &a1, &a2, &a3, &a4);
		coor[i].x = a1;
		coor[i].y[0] = a2;
		coor[i].z[0] = a3;
		if (a4 > L-3.0) coor[i].p = a4; else coor[i].p = L-1.1;
/////////////////////////
		coor[i].p = L-1.0;
/////////////////////////
        fscanf(loading_file, "%le %le %le\n", &a1, &a2, &a3);
		coor[i].vx = a1;
		coor[i].vy = a2;
		coor[i].vz = a3;
		coor[i].NOBR = 0;
		coor[i].y[1] = -10.0;
		coor[i].y[2] = -10.0;
		coor[i].y[3] = -10.0;
		if (coor[i].y[0] < 1.0 ) {
			coor[i].y[1] = coor[i].y[0] + A;
			coor[i].z[1] = coor[i].z[0];
			coor[i].NOBR = 1;
			coor[i].OBR = 1;
		} else if (coor[i].y[0] > A - 1.0 ) {
			coor[i].y[1] = coor[i].y[0] - A;
            coor[i].z[1] = coor[i].z[0];
			coor[i].NOBR = 1;
			coor[i].OBR = 1;
        }

		if (coor[i].z[0] < 1.0 ) {
			coor[i].z[2] = coor[i].z[0] + A;
			coor[i].y[2] = coor[i].y[0];

			if (coor[i].NOBR == 0) {
				coor[i].NOBR = 1;
				coor[i].OBR = 2;
			} else coor[i].NOBR = 3;
		} else if (coor[i].z[0] > A - 1.0 ) {
			coor[i].z[2] = coor[i].z[0] - A;
            coor[i].y[2] = coor[i].y[0];

			if (coor[i].NOBR == 0) {
				coor[i].NOBR = 1;
				coor[i].OBR = 2;
			} else coor[i].NOBR = 3;
        }
        if ( coor[i].NOBR == 3 ) {
			coor[i].y[3] = coor[i].y[1];
			coor[i].z[3] = coor[i].z[2];
		}
	}

	fclose(loading_file);
	double etta = (4.0 * PI * N) / (3.0 * A * A * (L - 2.0) );
	cube = exp( (1.0 / 3.0 ) * log( 2.0 * PI / etta ) );   // половина ребра куба,
                                                        // в котором вычисляются столкновения
	cube = cube + 4.0;
	printf("pol_rebra=%f\n", cube);
	printf("etta=%f\n", etta);
}

/////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////

// процедура рассчета близжайших событий для молекул из послания(list)
// Nlist - количество элементов в послании
// list - послание (содержит номера молекул, для которых надо рассчитать близжайшее событие)
// mt - количество элементов в линейке времен
// tm,im,lm,jm,km - линейка времен.
void retime(int &Nlist, int *list, int &mt, double *tm, int *im, int *lm, int *jm, int *km) {
	static int i, ii, kk, jj, mm, mi, ml, mj, mk, sd;
	static double tmin, dt;
	static double ax,ay,az,avx,avy,avz;
	static double bx,by,bz,bvx,bvy,bvz;
	static double bij,dr,dv,Dis;

	for (sd = 0; sd < Nlist; sd++) {    // до конца послания
		// запоминаем первую частицу в list[] для расчетавремени ее ближайшего события
 		mi = list[sd];
        ml = 0; mk = -1;
		tmin = 1.0E+20;
		ax = coor[mi].x; ay = coor[mi].y[0]; az = coor[mi].z[0];
		avx = coor[mi].vx; avy = coor[mi].vy; avz = coor[mi].vz;

        // начинаем рассчет для границ
		if (avx < 0.0) {           /// X = 1
			dt = (1.0 - ax) / avx; if ( (dt < tmin) && (dt > 0.0) ) { tmin = dt; mj = 8; }
		} else {                   /// X = L-1
			dt = (L - 1.0 - ax) / avx; if ( (dt < tmin) && (dt > 0.0) ) { tmin = dt; mj = 9; }
            //dt = (coor[mi].p - ax) / avx; if ( (dt < tmin) && (dt > 0.0) ) { tmin = dt; mj = 10; }
		}

		if (avy > 0.0) {                       /// Y = A;
			dt = (A - ay) / avy; if ( (dt < tmin) && (dt > 0.0) ) { tmin = dt; mj = 0; }
            /// Y = A-1
            dt = (A - 1.0 - ay) / avy; if ( (dt < tmin) && (dt > 0.0) ) { tmin = dt; mj = 1; }
            /// Y = 1
            dt = (1.0 - ay) / avy; if ( (dt < tmin) && (dt > 0.0) ) { tmin = dt; mj = 3; }
		} else {                      /// Y = 0
			dt = -ay / avy; if ( (dt < tmin) && (dt > 0.0) ) { tmin = dt; mj = 2; }
            /// Y = 1
            dt = (1.0 - ay) / avy; if ( (dt < tmin) && (dt > 0.0) ) { tmin = dt; mj = 3; }
			/// Y = A-1
            dt = (A - 1.0 - ay) / avy; if ( (dt < tmin) && (dt > 0.0) ) { tmin = dt; mj = 1; }
		}

		if (avz > 0.0 ) {                   /// Z = A
			dt = (A - az) / avz; if ( (dt < tmin) && (dt > 0.0 ) ) { tmin = dt; mj = 4; }
            /// Z = A-1
            dt = (A - 1.0 - az) / avz; if ( (dt < tmin) && (dt > 0.0 ) ) { tmin = dt; mj = 5; }
			/// Z = 1
            dt = (1.0 - az) / avz; if ( (dt < tmin) && (dt > 0.0 ) ) { tmin = dt; mj = 7; }
		} else {                /// Z = 0
			dt = -az / avz; if ( (dt < tmin) && (dt > 0.0 ) ) { tmin = dt; mj = 6; }
            /// Z = 1
            dt = (1.0 - az) / avz; if ( (dt < tmin) && (dt > 0.0 ) ) { tmin = dt; mj = 7; }
            /// Z = A-1
            dt = (A - 1.0 - az) / avz; if ( (dt < tmin) && (dt > 0.0 ) ) { tmin = dt; mj = 5; }
		}

////////////////////////////////
////// Рассчет столкновений
////// молекула-молекула
////// молекула-образ
////// образ-образ
////////////////////////////////
		if (coor[mi].NOBR == 0)	{	// если у нашей частицы нет образов(чаще всего выполняется это условие)
			// то рассматриваем только соударения частицы с другими частицами и с образами других частиц

			for (i = 0; i < mi; i++) {	// просматривем в первом цикле частицы от 1 до mi, а в следующем от mi+1 до N
				bx = coor[i].x - ax;
				if ( fabs(bx) > cube ) continue;
				if (coor[i].NOBR == 0) {
					by = coor[i].y[0] - ay;
					if (fabs(by) < cube) {
						bz = coor[i].z[0] - az;
						if (fabs(bz) < cube) {
							bvx = coor[i].vx - avx;
							bvy = coor[i].vy - avy;
							bvz = coor[i].vz - avz;
							bij = bx*bvx + by*bvy + bz*bvz;
							if (bij < 0.0) {
								dr = bx*bx + by*by + bz*bz;
								dv = bvx*bvx + bvy*bvy + bvz*bvz;
								Dis = bij*bij + dv*(4.0 - dr);
								if (Dis > 0.0) {
									dt = ( -bij - sqrt(Dis) ) / dv;
									if ( (dt < tmin) && (dt > 0.0) ) {
										tmin = dt;
										ml = 0; mj = i; mk = 0;
									}
								}
							}
						}
					}
				} //end if
				else if (coor[i].NOBR == 1) {	// если у частицы один образ
					// то необходимо рассмотреть соударения для самой частицы и ее образа
					kk = 0;	// kk сначала равно 0 (сама частица), а потом номеру образа
					for (ii = 0; ii < 2; ii++) {
						by = coor[i].y[kk] - ay;
						if (fabs(by) < cube) {
							bz = coor[i].z[kk] - az;
							if (fabs(bz) < cube) {
								bvx = coor[i].vx - avx;
								bvy = coor[i].vy - avy;
								bvz = coor[i].vz - avz;
								bij = bx*bvx + by*bvy + bz*bvz;
								if (bij < 0.0) {
									dr = bx*bx + by*by + bz*bz;
									dv = bvx*bvx + bvy*bvy + bvz*bvz;
									Dis = bij*bij + dv*(4.0 - dr);
									if (Dis > 0.0) {
										dt = ( -bij - sqrt(Dis) ) / dv;
										if ( (dt < tmin) && (dt > 0.0 ) ) {
											tmin = dt;
											ml = 0; mj = i; mk = kk;
										}
									}
								}
							}
						}
						kk = coor[i].OBR;
					}	// end for
				}	// end if
				else {	// если у частицы три образа, то надо рассмотреть все возможные соударения
					for (ii = 0; ii < 4; ii++) {
						by = coor[i].y[ii] - ay;
						if (fabs(by) < cube) {
							bz = coor[i].z[ii] - az;
							if (fabs(bz) < cube) {
								bvx = coor[i].vx - avx;
								bvy = coor[i].vy - avy;
								bvz = coor[i].vz - avz;
								bij = bx*bvx + by*bvy + bz*bvz;
								if (bij < 0.0) {
									dr = bx*bx + by*by + bz*bz;
									dv = bvx*bvx + bvy*bvy + bvz*bvz;
									Dis = bij*bij + dv*(4.0 - dr);
									if (Dis > 0.0) {
										dt = ( -bij - sqrt(Dis) ) / dv;
										if ( (dt < tmin) && (dt > 0.0 ) ) {
											tmin = dt;
											ml = 0; mj = i; mk = ii;
										}
									}
								}
							}
						}
					}	// end for
				}
			}	// конец цикла for (int i = 0; i < mi; i++)

			// разбиение на два цикла помогает избежать операций лишнего сравнения.
			for (i = mi+1; i < N; i++) {
				bx = coor[i].x - ax;
				if ( fabs(bx) > cube ) continue;
				if (coor[i].NOBR == 0) {
					by = coor[i].y[0] - ay;
					if (fabs(by) < cube) {
						bz = coor[i].z[0] - az;
						if (fabs(bz) < cube) {
							bvx = coor[i].vx - avx;
							bvy = coor[i].vy - avy;
							bvz = coor[i].vz - avz;
							bij = bx*bvx + by*bvy + bz*bvz;
							if (bij < 0.0) {
								dr = bx*bx + by*by + bz*bz;
								dv = bvx*bvx + bvy*bvy + bvz*bvz;
								Dis = bij*bij + dv*(4.0 - dr);
								if (Dis > 0.0) {
									dt = ( -bij - sqrt(Dis) ) / dv;
									if ( (dt < tmin) && (dt > 0.0) ) {
										tmin = dt;
										ml = 0; mj = i; mk = 0;
									}
								}
							}
						}
					}
				} //end if
				else if (coor[i].NOBR == 1) {
					kk = 0;
					for (ii = 0; ii < 2; ii++) {
						by = coor[i].y[kk] - ay;
						if (fabs(by) < cube) {
							bz = coor[i].z[kk] - az;
							if (fabs(bz) < cube) {
								bvx = coor[i].vx - avx;
								bvy = coor[i].vy - avy;
								bvz = coor[i].vz - avz;
								bij = bx*bvx + by*bvy + bz*bvz;
								if (bij < 0.0) {
									dr = bx*bx + by*by + bz*bz;
									dv = bvx*bvx + bvy*bvy + bvz*bvz;
									Dis = bij*bij + dv*(4.0 - dr);
									if (Dis > 0.0) {
										dt = ( -bij - sqrt(Dis) ) / dv;
										if ( (dt < tmin) && (dt > 0.0) ) {
											tmin = dt;
											ml = 0; mj = i; mk = kk;
										}
									}
								}
							}
						}
						kk = coor[i].OBR;
					}	// end for
				}	// end if
				else {
					for (ii = 0; ii < 4; ii++) {
						by = coor[i].y[ii] - ay;
						if (fabs(by) < cube) {
							bz = coor[i].z[ii] - az;
							if (fabs(bz) < cube) {
								bvx = coor[i].vx - avx;
								bvy = coor[i].vy - avy;
								bvz = coor[i].vz - avz;
								bij = bx*bvx + by*bvy + bz*bvz;
								if (bij < 0.0) {
									dr = bx*bx + by*by + bz*bz;
									dv = bvx*bvx + bvy*bvy + bvz*bvz;
									Dis = bij*bij + dv*(4.0 - dr);
									if (Dis > 0.0) {
										dt = ( -bij - sqrt(Dis) ) / dv;
										if ( (dt < tmin) && (dt > 0.0) ) {
											tmin = dt;
											ml = 0; mj = i; mk = ii;
										}
									}
								}
							}
						}
					}	// end for
				}
			}	// конец цикла for (int i = mi+1; i < N; i++)
		}	// end if
		else if (coor[mi].NOBR == 1) {	// если у частицы 1 образ (такое бывает в 1 из 7 случаев)
			mm = 0;
			for (jj = 0; jj < 2; jj++) {
				for (i = 0; i < mi; i++) {
					bx = coor[i].x - ax;
					if ( fabs(bx) > cube ) continue;
					if (coor[i].NOBR == 0) {
						by = coor[i].y[0] - coor[mi].y[mm];
						if (fabs(by) < cube) {
							bz = coor[i].z[0] - coor[mi].z[mm];
							if (fabs(bz) < cube) {
								bvx = coor[i].vx - avx;
								bvy = coor[i].vy - avy;
								bvz = coor[i].vz - avz;
								bij = bx*bvx + by*bvy + bz*bvz;
								if (bij < 0.0) {
									dr = bx*bx + by*by + bz*bz;
									dv = bvx*bvx + bvy*bvy + bvz*bvz;
									Dis = bij*bij + dv*(4.0 - dr);
									if (Dis > 0.0) {
										dt = ( -bij - sqrt(Dis) ) / dv;
										if ( (dt < tmin) && (dt > 0.0) ) {
											tmin = dt;
											ml = mm; mj = i; mk = 0;
										}
									}
								}
							}
						}
					} //end if
					else if (coor[i].NOBR == 1) {
						kk = 0;
						for (ii = 0; ii < 2; ii++) {
							by = coor[i].y[kk] - coor[mi].y[mm];
							if (fabs(by) < cube) {
								bz = coor[i].z[kk] - coor[mi].z[mm];
								if (fabs(bz) < cube) {
									bvx = coor[i].vx - avx;
									bvy = coor[i].vy - avy;
									bvz = coor[i].vz - avz;
									bij = bx*bvx + by*bvy + bz*bvz;
									if (bij < 0.0) {
										dr = bx*bx + by*by + bz*bz;
										dv = bvx*bvx + bvy*bvy + bvz*bvz;
										Dis = bij*bij + dv*(4.0 - dr);
										if (Dis > 0.0) {
											dt = ( -bij - sqrt(Dis) ) / dv;
											if ( (dt < tmin) && (dt > 0.0) ) {
												tmin = dt;
												ml = mm;
												mj = i;
												mk = kk;
											}
										}
									}
								}
							}
							kk = coor[i].OBR;
						}	// end for
					}	// end if
					else {
						for (ii = 0; ii < 4; ii++) {
							by = coor[i].y[ii] - coor[mi].y[mm];
							if (fabs(by) < cube) {
								bz = coor[i].z[ii] - coor[mi].z[mm];
								if (fabs(bz) < cube) {
									bvx = coor[i].vx - avx;
									bvy = coor[i].vy - avy;
									bvz = coor[i].vz - avz;
									bij = bx*bvx + by*bvy + bz*bvz;
									if (bij < 0.0) {
										dr = bx*bx + by*by + bz*bz;
										dv = bvx*bvx + bvy*bvy + bvz*bvz;
										Dis = bij*bij + dv*(4.0 - dr);
										if (Dis > 0.0) {
											dt = ( -bij - sqrt(Dis) ) / dv;
											if ( (dt < tmin) && (dt > 0.0) ) {
												tmin = dt;
												ml = mm; mj = i; mk = ii;
											}
										}
									}
								}
							}
						}	// end for
					}
				}	// конец цикла for (int i = 0; i < mi; i++)
				for (i = mi+1; i < N; i++) {
					bx = coor[i].x - ax;
					if ( fabs(bx) > cube ) continue;
					if (coor[i].NOBR == 0) {
						by = coor[i].y[0] - coor[mi].y[mm];
						if (fabs(by) < cube) {
							bz = coor[i].z[0] - coor[mi].z[mm];
							if (fabs(bz) < cube) {
								bvx = coor[i].vx - avx;
								bvy = coor[i].vy - avy;
								bvz = coor[i].vz - avz;
								bij = bx*bvx + by*bvy + bz*bvz;
								if (bij < 0.0) {
									dr = bx*bx + by*by + bz*bz;
									dv = bvx*bvx + bvy*bvy + bvz*bvz;
									Dis = bij*bij + dv*(4.0 - dr);
									if (Dis > 0.0) {
										dt = ( -bij - sqrt(Dis) ) / dv;
										if ( (dt < tmin) && (dt > 0.0) ) {
											tmin = dt;
											ml = mm; mj = i; mk = 0;
										}
									}
								}
							}
						}
					} //end if
					else if (coor[i].NOBR == 1) {
						kk = 0;
						for (ii = 0; ii < 2; ii++) {
							by = coor[i].y[kk] - coor[mi].y[mm];
							if (fabs(by) < cube) {
								bz = coor[i].z[kk] - coor[mi].z[mm];
								if (fabs(bz) < cube) {
									bvx = coor[i].vx - avx;
									bvy = coor[i].vy - avy;
									bvz = coor[i].vz - avz;
									bij = bx*bvx + by*bvy + bz*bvz;
									if (bij < 0.0) {
										dr = bx*bx + by*by + bz*bz;
										dv = bvx*bvx + bvy*bvy + bvz*bvz;
										Dis = bij*bij + dv*(4.0 - dr);
										if (Dis > 0.0) {
											dt = ( -bij - sqrt(Dis) ) / dv;
											if ( (dt < tmin) && (dt > 0.0) ) {
												tmin = dt;
												ml = mm; mj = i; mk = kk;
											}
										}
									}
								}
							}
							kk = coor[i].OBR;
						}	// end for
					}	// end if
					else {
						for (ii = 0; ii < 4; ii++) {
							by = coor[i].y[ii] - coor[mi].y[mm];
							if (fabs(by) < cube) {
								bz = coor[i].z[ii] - coor[mi].z[mm];
								if (fabs(bz) < cube) {
									bvx = coor[i].vx - avx;
									bvy = coor[i].vy - avy;
									bvz = coor[i].vz - avz;
									bij = bx*bvx + by*bvy + bz*bvz;
									if (bij < 0.0) {
										dr = bx*bx + by*by + bz*bz;
										dv = bvx*bvx + bvy*bvy + bvz*bvz;
										Dis = bij*bij + dv*(4.0 - dr);
										if (Dis > 0.0) {
											dt = ( -bij - sqrt(Dis) ) / dv;
											if ( (dt < tmin) && (dt > 0.0) ) {
												tmin = dt;
												ml = mm; mj = i; mk = ii;
											}
										}
									}
								}
							}
						}	// end for
					}
				}	// конец цикла for (int i = mi+1; i < N; i++)
				mm = coor[mi].OBR;
			}	// end for -> jj;
		}	// end if
		else {	// если у частицы 3 образа (такое бывает в 3 из 700 случаев)
			for (jj = 0; jj < 4; jj++) {
				for (i = 0; i < mi; i++) {
					bx = coor[i].x - ax;
					if ( fabs(bx) > cube ) continue;
					if (coor[i].NOBR == 0) {
						by = coor[i].y[0] - coor[mi].y[jj];
						if (fabs(by) < cube) {
							bz = coor[i].z[0] - coor[mi].z[jj];
							if (fabs(bz) < cube) {
								bvx = coor[i].vx - avx;
								bvy = coor[i].vy - avy;
								bvz = coor[i].vz - avz;
								bij = bx*bvx + by*bvy + bz*bvz;
								if (bij < 0.0) {
									dr = bx*bx + by*by + bz*bz;
									dv = bvx*bvx + bvy*bvy + bvz*bvz;
									Dis = bij*bij + dv*(4.0 - dr);
									if (Dis > 0.0) {
										dt = ( -bij - sqrt(Dis) ) / dv;
										if ( (dt < tmin) && (dt > 0.0) ) {
											tmin = dt;
											ml = jj; mj = i; mk = 0;
										}
									}
								}
							}
						}
					} //end if
					else if (coor[i].NOBR == 1) {
						kk = 0;
						for (ii = 0; ii < 2; ii++) {
							by = coor[i].y[kk] - coor[mi].y[jj];
							if (fabs(by) < cube) {
								bz = coor[i].z[kk] - coor[mi].z[jj];
								if (fabs(bz) < cube) {
									bvx = coor[i].vx - avx;
									bvy = coor[i].vy - avy;
									bvz = coor[i].vz - avz;
									bij = bx*bvx + by*bvy + bz*bvz;
									if (bij < 0.0) {
										dr = bx*bx + by*by + bz*bz;
										dv = bvx*bvx + bvy*bvy + bvz*bvz;
										Dis = bij*bij + dv*(4.0 - dr);
										if (Dis > 0.0) {
											dt = ( -bij - sqrt(Dis) ) / dv;
											if ( (dt < tmin) && (dt > 0.0) ) {
												tmin = dt;
												ml = jj; mj = i; mk = kk;
											}
										}
									}
								}
							}
							kk = coor[i].OBR;
						}	// end for
					}	// end if
					else {
						for (ii = 0; ii < 4; ii++) {
							by = coor[i].y[ii] - coor[mi].y[jj];
							if (fabs(by) < cube) {
								bz = coor[i].z[ii] - coor[mi].z[jj];
								if (fabs(bz) < cube) {
									bvx = coor[i].vx - avx;
									bvy = coor[i].vy - avy;
									bvz = coor[i].vz - avz;
									bij = bx*bvx + by*bvy + bz*bvz;
									if (bij < 0.0) {
										dr = bx*bx + by*by + bz*bz;
										dv = bvx*bvx + bvy*bvy + bvz*bvz;
										Dis = bij*bij + dv*(4.0 - dr);
										if (Dis > 0.0) {
											dt = ( -bij - sqrt(Dis) ) / dv;
											if ( (dt < tmin) && (dt > 0.0) ) {
												tmin = dt;
												ml = jj; mj = i; mk = ii;
											}
										}
									}
								}
							}
						}	// end for
					}
				}	// конец цикла for (int i = 0; i < mi; i++)
				for (i = mi+1; i < N; i++) {
					bx = coor[i].x - ax;
					if ( fabs(bx) > cube ) continue;
					if (coor[i].NOBR == 0) {
						by = coor[i].y[0] - coor[mi].y[jj];
						if (fabs(by) < cube) {
							bz = coor[i].z[0] - coor[mi].z[jj];
							if (fabs(bz) < cube) {
								bvx = coor[i].vx - avx;
								bvy = coor[i].vy - avy;
								bvz = coor[i].vz - avz;
								bij = bx*bvx + by*bvy + bz*bvz;
								if (bij < 0.0) {
									dr = bx*bx + by*by + bz*bz;
									dv = bvx*bvx + bvy*bvy + bvz*bvz;
									Dis = bij*bij + dv*(4.0 - dr);
									if (Dis > 0.0) {
										dt = ( -bij - sqrt(Dis) ) / dv;
										if ( (dt < tmin) && (dt > 0.0) ) {
											tmin = dt;
											ml = jj; mj = i; mk = 0;
										}
									}
								}
							}
						}
					} //end if
					else if (coor[i].NOBR == 1) {
						kk = 0;
						for (ii = 0; ii < 2; ii++) {
							by = coor[i].y[kk] - coor[mi].y[jj];
							if (fabs(by) < cube) {
								bz = coor[i].z[kk] - coor[mi].z[jj];
								if (fabs(bz) < cube) {
									bvx = coor[i].vx - avx;
									bvy = coor[i].vy - avy;
									bvz = coor[i].vz - avz;
									bij = bx*bvx + by*bvy + bz*bvz;
									if (bij < 0.0) {
										dr = bx*bx + by*by + bz*bz;
										dv = bvx*bvx + bvy*bvy + bvz*bvz;
										Dis = bij*bij + dv*(4.0 - dr);
										if (Dis > 0.0) {
											dt = ( -bij - sqrt(Dis) ) / dv;
											if ( (dt < tmin) && (dt > 0.0) ) {
												tmin = dt;
												ml = jj; mj = i; mk = kk;
											}
										}
									}
								}
							}
							kk = coor[i].OBR;
						}	// end for
					}	// end if
					else {
						for (ii = 0; ii < 4; ii++) {
							by = coor[i].y[ii] - coor[mi].y[jj];
							if (fabs(by) < cube) {
								bz = coor[i].z[ii] - coor[mi].z[jj];
								if (fabs(bz) < cube) {
									bvx = coor[i].vx - avx;
									bvy = coor[i].vy - avy;
									bvz = coor[i].vz - avz;
									bij = bx*bvx + by*bvy + bz*bvz;
									if (bij < 0.0) {
										dr = bx*bx + by*by + bz*bz;
										dv = bvx*bvx + bvy*bvy + bvz*bvz;
										Dis = bij*bij + dv*(4.0 - dr);
										if (Dis > 0.0) {
											dt = ( -bij - sqrt(Dis) ) / dv;
											if ( (dt < tmin) && (dt > 0.0) ) {
												tmin = dt;
												ml = jj; mj = i; mk = ii;
											}
										}
									}
								}
							}
						}	// end for
					}
				}	// конец цикла for (int i = mi+1; i < N; i++)
			}	// end for -> jj;
		}	// end else()

/////////////////////////////////// вставка нового события в линейку времен
		for (i = 0; i <= mt; i++)
			if (tmin < tm[i]) { // находим место в линейке времен (переделать бинарным поиском?)
				// проверка "соседа слева"
                // если он копия, то новое событие не вставляется в линейку времен
				if ( (i > 0) && (
				( (im[i-1] == mj) && (jm[i-1] == mi) && (km[i-1] == ml) && (lm[i-1] == mk)  )  ||
				( (im[i-1] == mi) && (jm[i-1] == mj) && (km[i-1] == mk) && (lm[i-1] == ml) )  ) )
					break;

				// вставка события
                // передвигаем все элементы, начиная с i-того, вправо
				for (ii = mt, kk=mt-1; ii > i; ii--, kk--) {
					tm[ii] = tm[kk];
					im[ii] = im[kk]; lm[ii] = lm[kk];
					jm[ii] = jm[kk]; km[ii] = km[kk];
				}
				mt++;
				tm[mt] = 1.0E+20;

                // вставка нового события
				tm[i] = tmin;
				im[i] = mi; lm[i] = ml;
				jm[i] = mj; km[i] = mk;

				break;
			}
	}
	// после обработки всех частиц из list[] обнуляем длину list[]
	Nlist = 0;
}


/////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////

 // процедура очистки линейки времен от участников произошедшего события.
 // в качестве оргументов получает:
 // Nlist - количество элементов послания( 1 или 2 )
 // list - послание (номера молекул, с которыми произошло событие)
 // для элементов из послания надо вычистить линейку времен
 // и запомнить всех соучастников удаляемых событий.
 // mt - количество элементов в линейке времен
 // tm,im,lm,jm,km - линейка времен
void purge(int &Nlist, int *list, int &mt, double *tm, int *im, int *lm, int *jm, int *km) {
	static int m, i, j, p, k;
	m = Nlist;		// запоминаем первоначальное количество элементов в послании
	for (i = 0; i < m; i++) {          // для каждого элемента первоначального послания
        j = 0;	// номер просматриваемого события
		while (j < mt) {               // чистим линейку времен
			// просматриваем массив im[] на наличие частиц, для которых произошло событие
			if (im[j] == list[i]) {
				if (km[j] >= 0) {	// если в недействительном событии участвует вторая частица
					// то добавить ее в list[] для процедуры перерасчета времени ближайшего события
					list[ Nlist ] = jm[j];
					Nlist++;
				}
				// удаляем найденное недействительное событие из линейки времен
				for (p = j, k=j+1; p < mt; p++,k++) {
					tm[p] = tm[k];
					im[p] = im[k]; lm[p] = lm[k];
					jm[p] = jm[k]; km[p] = km[k];
				}
				mt--;
                tm[mt] = 1.0E+20;
				continue;
			}
			// просматриваем массив jm[] на наличие частиц, для которых произошло событие
			if ( (jm[j] == list[i]) && (km[j] >= 0) ) {
				// добавляем вторую частицу, участвующую в недействительном событии в list[] для
				// процедуры перерасчета времени ближайшего события
				list[ Nlist ] = im[j];
				Nlist++;
				// удаляем найденное недействительное событие из линейки времен
				for (p = j, k=j+1; p < mt; p++,k++) {
					tm[p] = tm[k];
					im[p] = im[k]; lm[p] = lm[k];
					jm[p] = jm[k]; km[p] = km[k];
				}
				mt--;
                tm[mt] = 1.0E+20;
				continue;
			}
			// если не удаляли события из линейки времен, то переходим к следующему
			// если удаляли, то номер удаленного совпадает с номером следующего и
			// увеличивать j не нужно
            j++;
		}
    }
}


////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////

// процедура глобальной очистки.
// полностью обнуляет линейку времен
// заполняет послание к ретайму номерами всех молекул
// для рассчета их близжайших событий.
void purgeX(int &Nlist, int *list, int &mt, double *tm, int *im, int *lm, int *jm, int *km) {
	int i;
	for (i = 0; i < N_MAX; i++) { list[i] = 0; tm[i] = 1.0E+20; }
	for (i = 0; i < N; i++) list[i] = i;
	Nlist = N;
	mt = 0;
}

////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////

// процедура перерождения молекулы.
// молекула становится образом, образ становится молекулой
// на границе 0 или А.
// если у молекулы были другие образы, то они меняются местами.
// mi - номер молекулы
// mj - номер стенки на которой произошло перерождение
void reName(int mi, int mj) {
	static double k;
	if (mj < 4) {    // если mj<4 - то это образ по Y
		k = coor[mi].y[0];				// запоминаем координаты образа
		coor[mi].y[0] = coor[mi].y[1];	// координату образа приравниваем координате молекулы
		coor[mi].y[1] = k;				// задаем новую координату молекулы
        coor[mi].y[2] = coor[mi].y[0];	// задаем координату для 2го
        coor[mi].y[3] = coor[mi].y[1];	// и 3го образа
	} else {        // иначе это образ по Z
		k = coor[mi].z[0];				// запоминаем координаты образа
		coor[mi].z[0] = coor[mi].z[2];	// координату образа приравниваем координате молекулы
		coor[mi].z[2] = k;				// задаем новую координату молекулы
        coor[mi].z[1] = coor[mi].z[0];	// задаем координату для 1го
        coor[mi].z[3] = coor[mi].z[2];	// и 3го образа
	}
}

////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////

// процедура рассчета скоростей молекул после произошедшего столкновения
// получает в качестве параметров:
// mi - номер первой из столкнувшихся молекул
// ml - номер образа первой молекулы
// mj - номер второй из столкнувшихся молекул
// mk - номер образа второй молекулы
void collision(int mi, int ml, int mj, int mk) {
	static double v1x,v1y,v1z,v2x,v2y,v2z;
	static double rx,ry,rz;
	static double q1,q2;
	static double z1,z2;
	v1x = coor[mi].vx;      //
	v1y = coor[mi].vy;      //  запоминаем скорости первой молекулы
	v1z = coor[mi].vz;      //
	v2x = coor[mj].vx;      //
	v2y = coor[mj].vy;      //  запоминаем скорости второй молекулы
	v2z = coor[mj].vz;      //
    // разница координат по трем осям
	rx = coor[mi].x - coor[mj].x;
	ry = coor[mi].y[ml] - coor[mj].y[mk];
	rz = coor[mi].z[ml] - coor[mj].z[mk];
	z1 = rx*rx + ry*ry + rz*rz;
    // данные множители определяют множитель переданного импульса
	q1 = (rx*v1x + ry*v1y + rz*v1z) / z1; // 4.0; // множитель первой молекулы
	q2 = (rx*v2x + ry*v2y + rz*v2z) / z1; // 4.0; // множитель второй молекулы
    // задание новых скоростей молекул
	z1 = q2-q1;
	coor[mi].vx += rx * z1;
	coor[mi].vy += ry * z1;
	coor[mi].vz += rz * z1;
	z2 = q1-q2;
	coor[mj].vx += rx * z2;
	coor[mj].vy += ry * z2;
	coor[mj].vz += rz * z2;

	// данный блок необходим для расчета давления
/*
	if ((ml == 0) && (mk == 0) &&
		(coor[mi].x > x1) && (coor[mj].x > x1) &&
		(coor[mi].x < x2) && (coor[mj].x < x2)) CR++;
	if (CR >= 1000*Npress) STOP = true;
*/
}

////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////

///////////////////////////////
// процедура создания образа.
// образ молекулы создается при пересечении ей границ 1 или А-1
// процедура получает в качестве параметров:
// mi - номер молекулы, у которой рождается образ
// mj - номер стенки, на которой рождается образ
// примечание - уточнение координат помогает избезать ошибок из за неточности в 13-14 знаке.
void create(int mi, int mj) {
	switch (mj) {
		case 1:     // молекула на границе Y = А-1
			// уточняем координаты частицы
			coor[mi].y[1] = -1.0; coor[mi].z[1] = coor[mi].z[0];
			// если у частицы нет образов, то создаем один образ, задавая ее координаты
			if ( coor[mi].NOBR == 0 ) { coor[mi].NOBR = 1; coor[mi].OBR = 1; } else
			// иначе у частицы есть образ, создаем второй образ и третий, задавая их координаты
			{ coor[mi].y[3] = coor[mi].y[1]; coor[mi].z[3] = coor[mi].z[2]; coor[mi].NOBR = 3; }
		break;
		case 3:     // молекула на границе Y = 1
			// уточняем координаты частицы
			coor[mi].y[1] = A + 1.0; coor[mi].z[1] = coor[mi].z[0];
			// если у частицы нет образов, то создаем один образ, задавая ее координаты
			if ( coor[mi].NOBR == 0 ) { coor[mi].NOBR = 1; coor[mi].OBR = 1; } else
			// иначе у частицы есть образ, создаем второй образ и третий, задавая их координаты
			{ coor[mi].y[3] = coor[mi].y[1]; coor[mi].z[3] = coor[mi].z[2]; coor[mi].NOBR = 3; }
		break;
		case 5:     // молекула на границе Z = A-1
			// уточняем координаты частицы
			coor[mi].z[2] = -1.0; coor[mi].y[2] = coor[mi].y[0];
			// если у частицы нет образов, то создаем один образ, задавая ее координаты
			if ( coor[mi].NOBR == 0 ) { coor[mi].NOBR = 1; coor[mi].OBR = 2; } else
			// иначе у частицы есть образ, создаем второй образ и третий, задавая их координаты
			{ coor[mi].y[3] = coor[mi].y[1]; coor[mi].z[3] = coor[mi].z[2]; coor[mi].NOBR = 3; }
		break;
		case 7:     // молекула на границе Z = 1
			// уточняем координаты частицы
			coor[mi].z[2] = A + 1.0; coor[mi].y[2] = coor[mi].y[0];
			// если у частицы нет образов, то создаем один образ, задавая ее координаты
			if ( coor[mi].NOBR == 0 ) { coor[mi].NOBR = 1; coor[mi].OBR = 2; } else
			// иначе у частицы есть образ, создаем второй образ и третий, задавая их координаты
			{ coor[mi].y[3] = coor[mi].y[1]; coor[mi].z[3] = coor[mi].z[2]; coor[mi].NOBR = 3; }
		break;
	}
}


/////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////

void reform(int &mi, int &ml, int &mj, int &mk) {
    //static int N1;
	if (mk < 0) {
		switch (mj) {
			case 0:         // Y = A;
                coor[mi].y[0] = A;
                coor[mi].y[1] = 0.0;
				reName(mi,mj);
			break;
			case 1:         // Y = A - 1;
                coor[mi].y[0] = A - 1.0;
				if (coor[mi].vy > 0.0) create(mi, mj);
				else if ( coor[mi].NOBR == 1 ) coor[mi].NOBR = 0;
					else { coor[mi].NOBR = 1; coor[mi].OBR = 2; }
			break;
			case 2:        // Y = 0;
                coor[mi].y[0] = 0.0;
                coor[mi].y[1] = A;
				reName(mi,mj);
			break;
			case 3:      // Y = 1;
                coor[mi].y[0] = 1.0;
				if (coor[mi].vy < 0.0) create(mi, mj);
				else if ( coor[mi].NOBR == 1 ) coor[mi].NOBR = 0;
					else { coor[mi].NOBR = 1; coor[mi].OBR = 2; }
			break;
			case 4:    // Z = A;
                coor[mi].z[0] = A;
                coor[mi].z[2] = 0.0;
				reName(mi,mj);
			break;
			case 5:     // Z = A -1;
                coor[mi].z[0] = A - 1.0;
				if (coor[mi].vz > 0.0) create(mi, mj);
				else if ( coor[mi].NOBR == 1 ) coor[mi].NOBR = 0;
					else { coor[mi].NOBR = 1; coor[mi].OBR = 1; }
			break;
			case 6:     // Z = 0;
                coor[mi].z[0] = 0.0;
                coor[mi].z[2] = A;
				reName(mi,mj);
			break;
			case 7:       // Z = 1;
                coor[mi].z[0] = 1.0;
				if (coor[mi].vz < 0.0) create(mi, mj);
				else if ( coor[mi].NOBR == 1 ) coor[mi].NOBR = 0;
					else { coor[mi].NOBR = 1; coor[mi].OBR = 1; }
			break;
			case 8:        // X = 1;
                coor[mi].x = 1.0;
				coor[mi].vx *= -1.0;
			break;
			case 9:       // X = L - 3;
                coor[mi].x = L-1.0;
                if (coor[mi].vx > 0) coor[mi].vx *= -1.0;
			break;
            case 10:
                coor[mi].x = L-1.0;
                if (coor[mi].vx > 0) coor[mi].vx *= -1.0;
			break;
		}
	} else collision(mi, ml, mj, mk);
}

/////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////
void step(int &Nlist, int *list, int &mt, double *tm, int *im, int *lm, int *jm, int *km) {
	static double dt, ay, az;
	static int i, j, jj, mi, ml, mj, mk;
	static int N2 = N/2;
	jj = 0;
	while ( (jj < N2) && (STOP==false) ) {
		dt = tm[0];
		mi = im[0]; ml = lm[0];
		mj = jm[0]; mk = km[0];

		Time = Time+dt;

//		#pragma omp parallel for private(ay,az)
		for (i = 0; i < N; i++) {
			//процедура удаления частицы из массивов времен(в кристалах)
			Del_mas(i);

			// передвигаем частицу
			ay = coor[i].vy*dt;
			coor[i].y[0] += ay;
			az = coor[i].vz*dt;
			coor[i].z[0] += az;
			coor[i].x = coor[i].x + coor[i].vx*dt;
			// если у частицы нет образов переходим к следующей частице
			if ( coor[i].NOBR == 0 ) continue;
			else if ( coor[i].NOBR == 1 ) {		// если у частицы один образ, то его надо сдвинуть
				coor[i].y[1] += ay;
				coor[i].z[1] += az;
				coor[i].y[2] += ay;
				coor[i].z[2] += az;
			}
			else {	// иначе у частицы три образа и их надо сдвинуть.
				coor[i].y[1] += ay;
				coor[i].z[1] += az;
				coor[i].y[2] += ay;
				coor[i].z[2] += az;
				coor[i].y[3] += ay;
				coor[i].z[3] += az;
			}
		}
		// удаляем первое событие из линейки времен
    	mt--;
		for (i = 0, j=1; i < mt; i++, j++) {
			tm[i] = tm[j] - dt;
			im[i] = im[j]; lm[i] = lm[j];
			jm[i] = jm[j]; km[i] = km[j];
		}
		tm[mt] = 1.0E+20;
		// добавляем частицу-участницу события в list[] к процедуре пересчета
		list[ Nlist ] = mi;
		Nlist++;
		// если в событии участвовало две частицы, добавляем в list[] и вторую частицу
		if (mk >= 0) { list[ Nlist ] = mj; Nlist++; jj++; }
		// производим изменения в системе в соответствии с произошедшим событием
		reform(mi, ml, mj, mk);
		// очищаем линейку времен от недействительных событий
		purge(Nlist, list, mt, tm, im, lm, jm, km);
/*
		// ускорение.
		while ( tm[0] < 1.0E-14 ) {
			mi = im[0]; ml = lm[0];
			mj = jm[0]; mk = km[0];

			mt--;
			for (i = 0, j=1; i < mt; i++, j++) {
				tm[i] = tm[j] - dt;
				im[i] = im[j]; lm[i] = lm[j];
				jm[i] = jm[j]; km[i] = km[j];
			}

			tm[mt] = 1.0E+20;

			list[ Nlist ] = mi;
			Nlist++;

			if (mk >= 0) { list[ Nlist ] = mj; Nlist++; jj++; }

			reform(mi, ml, mj, mk);
			purge(Nlist, list, mt, tm, im, lm, jm, km);
		}
*/
		// расчитываем новые времена для определенных в list[] частиц
		retime(Nlist, list, mt, tm, im, lm, jm, km);
	}
}


//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

///  процедура сжатия. сжимает систему до необходимой плотности.
// получает в качестве оргументов:
// etta - плотность, до которой надо сжать систему
// Nlist - количество элементов в послании (нужно для передачи степу)
// list - послание (нужно для передачи степу)
// mt - количество элементов в линейке времен
// tm, im, lm, jm, km - элементы линейки времен (нужно для передачи степу)
void compress(double etta, int &Nlist, int *list, int &mt, double *tm, int *im, int *lm, int *jm, int *km) {
	int MaxK, MinK, i;
	double MaxX, MinX, dx;
	if ( etta > ( (4.0 * PI * N) / (3.0 * A * A * (L-2.0)) ) )
	{	// если надо уменьшить объем.
		while ( etta > ( (4.0 * PI * N) / (3.0 * A * A * (L-1.0)) ) ) {
			MaxK = 0;
			MinK = 0;
			MaxX = coor[0].x;
			MinX = coor[0].x;

			step(Nlist, list, mt, tm, im, lm, jm, km);

			for (i=0; i<N; i++) {
				if (coor[i].x < MinX) { MinX = coor[i].x; MinK = i; }
				if (coor[i].x > MaxX) { MaxX = coor[i].x; MaxK = i; }
			}

			if ((MinX-1.0) < (L-MaxX-1.0)) dx = MinX-1.0; else dx = L-MaxX-1.0;

			if (coor[MinK].vx < 0) coor[MinK].vx = -coor[MinK].vx;
			if (coor[MaxK].vx > 0) coor[MaxK].vx = -coor[MaxK].vx;

			printf("%f\n", dx);
			if (dx>0)
			{
				L = L - dx - dx;

				for (i=0; i<N; i++) {
					coor[i].x = coor[i].x - dx;
					coor[i].p = L-1.0;
				}
			}

			purgeX(Nlist, list, mt, tm, im, lm, jm, km);
			retime(Nlist, list, mt, tm, im, lm, jm, km);
		}
	}
	else
	{	// если надо увеличить объем
		L = 2.0 + (4.0 * PI * N) / (3.0 * A * A * etta );
	}
	printf("etta=%f\n", (4.0 * PI * N) / (3.0 * A * A * (L - 2.0) ) );
	printf("A=%f  L=%f\n", A, L);
}


//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////



///  альтернативная процедура сжатия. сжимает систему до необходимой плотности, изменяя значение А.
// получает в качестве оргументов:
// etta - плотность, до которой надо сжать систему
// Nlist - количество элементов в послании (нужно для передачи степу)
// list - послание (нужно для передачи степу)
// mt - количество элементов в линейке времен
// tm, im, lm, jm, km - элементы линейки времен (нужно для передачи степу)
/*
void compressA(double etta, int &Nlist, int *list, int &mt, double *tm, int *im, int *lm, int *jm, int *km) {
	int i;
	double maxA,minA;
	while ( etta > ( (4.0 * PI * N) / (3.0 * A * A * (L-1.0)) ) )
	{
		maxA = 0.0;
		minA = A;
		step(Nlist, list, mt, tm, im, lm, jm, km);
		step(Nlist, list, mt, tm, im, lm, jm, km);
		for (i = 0; i < N; i++)
		{
			if (coor[i].y[0] > maxA) maxA = coor[i].y[0];
			if (coor[i].z[0] > maxA) maxA = coor[i].z[0];
			if (coor[i].y[0] < minA) minA = coor[i].y[0];
			if (coor[i].z[0] < minA) minA = coor[i].z[0];
		}
		maxA = (A - maxA);
		if (minA < maxA) maxA = minA;
		A = A - maxA + 1.0E-7;

//////////////////////
		for (i=1; i<N; i++)
		{
			coor[i].NOBR = 0;
			coor[i].y[1] = -10.0;
			coor[i].y[2] = -10.0;
			coor[i].y[3] = -10.0;
			if (coor[i].y[0] < 1.0 ) {
				coor[i].y[1] = coor[i].y[0] + A;
				coor[i].z[1] = coor[i].z[0];
				coor[i].NOBR = 1;
				coor[i].OBR = 1;
			} else if (coor[i].y[0] > A - 1.0 ) {
				coor[i].y[1] = coor[i].y[0] - A;
		        coor[i].z[1] = coor[i].z[0];
				coor[i].NOBR = 1;
				coor[i].OBR = 1;
		    }

			if (coor[i].z[0] < 1.0 ) {
				coor[i].z[2] = coor[i].z[0] + A;
				coor[i].y[2] = coor[i].y[0];

				if (coor[i].NOBR == 0) {
					coor[i].NOBR = 1;
					coor[i].OBR = 2;
				} else coor[i].NOBR = 3;
			} else if (coor[i].z[0] > A - 1.0 ) {
				coor[i].z[2] = coor[i].z[0] - A;
		        coor[i].y[2] = coor[i].y[0];

				if (coor[i].NOBR == 0) {
					coor[i].NOBR = 1;
					coor[i].OBR = 2;
				} else coor[i].NOBR = 3;
		    }
		    if ( coor[i].NOBR == 3 ) {
				coor[i].y[3] = coor[i].y[1];
				coor[i].z[3] = coor[i].z[2];
			}
		}
//////////////////////

		purgeX(Nlist, list, mt, tm, im, lm, jm, km);
		retime(Nlist, list, mt, tm, im, lm, jm, km);
	}
	printf("etta=%f\n", (4.0 * PI * N) / (3.0 * A * A * (L - 1.0) ) );
	printf("A=%f  L=%f\n", A, L);
}
*/
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

// заготовка процедуры для определения двучастичной функции распределения
/*
void G2(int &Nlist, int *list, int &mt, double *tm, int *im, int *lm, int *jm, int *km)
{
	long int K[600];
	int i, ii , j, jj;	// jj - номер выбранной частицы
	int z;
	double x1;

	x1 = 20.0;

	for (i=0; i<600; i++) K[i]=0;

	for (i=0; i<10000; i++)
	{
		for (j=0;j<N;j++) if ( (coor[j].x>x1) && (coor[j].x<x1+2.0) &&
		( (coor[j].y[0]>10) && (coor[j].y[0]<15) && (coor[j].z[0]>10) && (coor[j].z[0]<15) ) ) jj = j;

		step(Nlist, list, mt, tm, im, lm, jm, km);

		for (j=0; j<N; j++)
		{
			if ( (j!=jj) && (8.0 > sqrt( (coor[j].x-coor[jj].x)*(coor[j].x-coor[jj].x) +
											(coor[j].y[0]-coor[jj].y[0])*(coor[j].y[0]-coor[jj].y[0]) +
											(coor[j].z[0]-coor[jj].z[0])*(coor[j].z[0]-coor[jj].z[0]) ) ) )
			{
					z = int(100*sqrtl( (coor[j].x-coor[jj].x)*(coor[j].x-coor[jj].x) +
											(coor[j].y[0]-coor[jj].y[0])*(coor[j].y[0]-coor[jj].y[0]) +
											(coor[j].z[0]-coor[jj].z[0])*(coor[j].z[0]-coor[jj].z[0]))) - 200;
					K[z] = K[z]+1;
			}
		}
	}

	FILE *g2_file;
	g2_file = fopen( "data.txt" , "w+" );
	for (i=0; i<600; i++)
		fprintf(g2_file, "%i\n", K[i]);

	fclose(g2_file);
}
*/

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

// Процедура измерения давления
// замеряется время, за которое в выделенном объеме произойдет Npress соударений
// это время подставляется в формулу (соотношение с временем для разбавленной системы)
void Pressure(int &Nlist, int *list, int &mt, double *tm, int *im, int *lm, int *jm, int *km)
{
	int i, j, k;

	// такая конструкция отключает подсчет произошедших соударений
	x1 = -20.0;
	x2 = 20;
	k = 0;
	// рассчитываем число частиц в выбранной области, чтобы в дальнейшем знать
	// сколько соударений измерять давление
	for (i = 1; i < 1000; i++)
	{
		step(Nlist, list, mt, tm, im, lm, jm, km);
		step(Nlist, list, mt, tm, im, lm, jm, km);
		for (j = 1; j < N; j++) if ((coor[j].x > x1) && (coor[j].x < x2)) k++;
	}
	Npress = k / 1000.0;

	printf("\n pressure start \n");

	// определяем плоскости, перпендикулярные OX, задающие область, в которой измеряется давление
	x1 = 20.0;
	x2 = L-20;

	k = 0;			// переменная для подсчета частиц в выбранной области
	i = 0;			// число замеров плотности в выделенной области
	CR = 0;			// число соударений в выбранной области
	Time = 0.0;		// время, за которое в выбранной области произойдет Npress соударений
	STOP = false;	// переменная для определения завершения измерения давления
	while (STOP == false) {
		step(Nlist, list, mt, tm, im, lm, jm, km);
		i++;
		for (j = 1; j < N; j++) if ((coor[j].x > x1) && (coor[j].x < x2)) k++;
	}

	// выводим среднюю плотность в области, для которой замерялось давление
	printf("\n\n etta liquid x>>[ %i; %i ] = %.15le", x1, x2, ((double)k/(double)i)*(4.0/3.0)*PI/(A*A*(x2-x1)) );
	// выводим время
	printf("\n Time = %.15le \n", Time);
	printf("Program complite");
}



// процедура проверок
// проверяет наличие всех молекул в линейке времен и проникновения
// получает в качестве оргументов:
// i - номер шага, на котором проводится проверка. для внеплановых проверок можно задавать отрицательные значения.
// mt - количество элементов в линейке времен
// tm,im,lm,jm,km - элементы линейки времен
void check(int ii, int &mt, double *tm, int *im, int *lm, int *jm, int *km) {
	static int i,j, b;
    static double r,rx,ry,rz;
	// проверка нахождения каждой частицы в линейке времен
	for (i = 0; i < N; i++) {
		b = 0;
		for (j = 0; j < mt; j++)
			if ( (im[j] == i) || ( (jm[j] == i) && (km[j] >= 0) ) ) b = 1;

		if (b == 0) {
			printf("ERROR: molekula %i ne v lineike!\n", i);
            fprintf(error_file, "На шаге %i молекула %i не находится в линейке времен.\n", ii, i);
        }
	}
    /// проверка на проникновение частиц
	for (i = 0; i < N-1; i++)
		for (j = i+1; j < N; j++) {
			rx = coor[i].x - coor[j].x;
			ry = coor[i].y[0] - coor[j].y[0];
			rz = coor[i].z[0] - coor[j].z[0];
			r = sqrt(rx*rx + ry*ry + rz*rz);
			if (r < 2.0 - 1.0E-14) {
				printf("ERROR: proniknovenie! %.5le\n", (2.0-r));
				fprintf(error_file, "На шаге %i произошло проникновение на %.15le.\n", ii, (2.0 - r));
				fprintf(error_file, "Проникновение между %i[] и %i[].\n", i, j);
			}
		}
    ///////////////////////////////////////////////////////////
    for (i = 0; i < N; i++)
		if ( (coor[i].x < 1.0) || (coor[i].x > L) ||
			(coor[i].y[0] > A) || (coor[i].y[0] < 0.0) ||
			(coor[i].z[0] > A) || (coor[i].z[0] < 0.0) ) {
				printf("ERROR: vilet! %f\n", coor[i].x);
				fprintf(error_file, "На шаге %i произошел вылет молекулы за пределы объема.", ii);
				fprintf(error_file, "Координаты молекулы %i:\n", i);
				fprintf(error_file, "x=%.15le  y=%.15le  z=%.15le.\n", coor[i].x, coor[i].y[0], coor[i].z[0]);
		}
    ////////////////////////////////////////////////////////////
	for (i = 0; i < mt-1; i++)
        if (tm[i] > tm[i+1]) {
			printf("ERROR: narushenie lineiki vremen!\n");
			fprintf(error_file, "Нарушение линейки времен. ШАГ %i\n", ii);
			fprintf(error_file, "%.15le  %.15le\n",tm[i], tm[i+1]);
        }
}

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

// Процедура проверки импульса системы
void impulse() {
	double px = 0.0;
	double py = 0.0;
	double pz = 0.0;
	for (int i = 0; i < N; i++) {
		px += coor[i].vx;
		py += coor[i].vy;
		pz += coor[i].vz;
	}
	fprintf(impulse_file, "px = %.15le  | py = %.15le  | pz = %.15le. \n\n", px, py, pz);

	double Mpx = 0.0;
	double Mpy = 0.0;
	double Mpz = 0.0;
	for (int i = 0; i < N; i++) {
		Mpx += (coor[i].y[0] - A/2.0)*(coor[i].vz) - (coor[i].z[0] - A/2.0)*(coor[i].vy);
		Mpy += (coor[i].z[0] - A/2.0)*(coor[i].vx) - (coor[i].x - L/2.0)*(coor[i].vz);
		Mpz += (coor[i].x - L/2.0)*(coor[i].vy) - (coor[i].y[0] - A/2.0)*(coor[i].vx);
	}
	fprintf(impulse_file, " Mpx = %.15le   Mpy = %.15le  Mpz = %.15le\n\n", Mpx, Mpy, Mpz);
}


//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

void revers(int &Nlist, int *list, int &mt, double *tm, int *im, int *lm, int *jm, int *km)
{
    coord coor1[N_MAX];

    for (int i=0; i<N; i++)
    {
        coor1[i].x = coor[i].x;
		coor1[i].y[0] = coor[i].y[0];
		coor1[i].z[0] = coor[i].z[0];

		coor1[i].vx = coor[i].vx;
		coor1[i].vy = coor[i].vy;
		coor1[i].vz = coor[i].vz;
	}

	FILE *do1_file = fopen("do1.txt", "w+");

	for (int i=0; i<5000; i++)
	{
		fprintf(do1_file," %.18Le %i %i %i %i\n", tm[0], im[0], lm[0], jm[0], km[0]);
		step(Nlist, list, mt, tm, im, lm, jm, km);
		//    proverka(-i, mt, tm, im, lm, jm, km);
	}

	fclose(do1_file);

/*
	for (int i=0; i<N; i++)
	{
		coor[i].x += coor[i].vx * (tm[0] * 0.90);
		for (int j=0; j<4; j++)
			{
				coor[i].y[j] += coor[i].vy * (tm[0] * 0.90);
				coor[i].z[j] += coor[i].vz * (tm[0] * 0.90);
			}
	}
*/
	for (int i=0; i<N; i++)
	{
		coor[i].vx *= -1.0;
		coor[i].vy *= -1.0;
		coor[i].vz *= -1.0;
	}

	purgeX(Nlist, list, mt, tm, im, lm, jm, km);
	retime(Nlist, list, mt, tm, im, lm, jm, km);

	FILE *do2_file = fopen("do2.txt", "w+");

	for (long int i=0; i<5000; i++)
	{
		fprintf(do2_file," %.18Le %i %i %i %i\n", tm[0], im[0], lm[0], jm[0], km[0]);
		step(Nlist, list, mt, tm, im, lm, jm, km);
		 //   proverka(-i, mt, tm, im, lm, jm, km);
	}

	fclose(do1_file);

/*
	for (int i=0; i<N; i++)
	{
		coor[i].x += coor[i].vx * (tm[0] * 0.90);
		for (int j=0; j<4; j++)
			{
				coor[i].y[j] += coor[i].vy * (tm[0] * 0.90);
				coor[i].z[j] += coor[i].vz * (tm[0] * 0.90);
			}
	}
*/

	FILE *revers_file = fopen("revers.txt", "w+");
	for (int i=0; i<N; i++)
	{
		fprintf(revers_file,"Молекула № %i\n",i);

		fprintf(revers_file,"x=%.15le | y=%.15le | z=%.15le\n",coor1[i].x - coor[i].x, coor1[i].y[0] - coor[i].y[0], coor1[i].z[0] - coor[i].z[0] );

		fprintf(revers_file,"vx=%.15le | vy=%.15le | vz=%.15le\n", (coor1[i].vx + coor[i].vx), (coor1[i].vy + coor[i].vy), (coor1[i].vz + coor[i].vz));

		fprintf(revers_file,"---------------\n");
	}
	fclose(revers_file);

}

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////


void image(int &Nlist, int *list, int &mt, double *tm, int *im, int *lm, int *jm, int *km) {
	int i, ii, j;
    int IMG[3000];
    double dx = 0.05;
    double x = 0.0;

    for(i = 0; i < 3000; i++) IMG[i] = 0;

    for (ii = 0; ii < 2000; ii++) {
		step(Nlist, list, mt, tm, im, lm, jm, km);
		//step(Nlist, list, mt, tm, im, lm, jm, km);
        j = 0;
        x = 0.0;
        while (x < L) {
            for (i = 0; i < N; i++)
                if ( (coor[i].x >= x) && (coor[i].x < x+dx) ) IMG[j] = IMG[j] + 1;
            x = x + dx;
			j = j+1;
        }
    }

    fprintf(image_file,"%i\n", (int)(L*20) );

    for (i = 0; i < L*20; i++)
        fprintf(image_file,"%i\n", IMG[i] );
}


//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////


void image_profile(int &Nlist, int *list, int &mt, double *tm, int *im, int *lm, int *jm, int *km) {
	double IMG_Y[200][500];
	double IMG_Z[200][500];
	int i, j, ii;

	for(i = 0; i < 200; i++)
		for (j = 0; j < 500; j++)
			IMG_Y[i][j] = IMG_Z[i][j] = 0.0;

	for (j = 0; j < 500; j++) {
		step(Nlist, list, mt, tm, im, lm, jm, km);
		i = 0;
		for (ii = 0; ii < N; ii++)
			if ( (coor[ii].x <= x2) && (coor[ii].x >= x1) ) {
				IMG_Y[i][j] = coor[ii].y[0];
				IMG_Z[i][j] = coor[ii].z[0];
				i++;
			}
	}

	for (i = 0; i < 200; i++)
		for (j = 0; j < 500; j++) {
			fprintf(image_profile_file,"%.15le\n", IMG_Y[i][j] );
			fprintf(image_profile_file,"%.15le\n", IMG_Z[i][j] );
		}
}


////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////


void gipD(int &Nlist, int *list, int &mt, double *tm, int *im, int *lm, int *jm, int *km) {
	x1=10;
	x2=-10;
	Npress = 110E+7;
	load_seed();
	purgeX(Nlist, list, mt, tm, im, lm, jm, km);
	retime(Nlist, list, mt, tm, im, lm, jm, km);
	check(1, mt, tm, im, lm, jm, km);
	for (int i=0; i<100; i++) step(Nlist, list, mt, tm, im, lm, jm, km);
	check(1, mt, tm, im, lm, jm, km);
////////////////////////
	int kN1,kN2,kN3;
	int NN1[200];
	int NN2[200];
	int NN3[200];

	double px1[200];
	double px2[200];
	double px3[200];

	double Ex1[200];
	double Ey1[200];
	double Ez1[200];

	double Ex2[200];
	double Ey2[200];
	double Ez2[200];

	double Ex3[200];
	double Ey3[200];
	double Ez3[200];
////////////////////////
	kN1 = 0;
	kN2 = 0;
	kN3 = 0;
	for (int i=0; i<N; i++) if ( coor[i].x<2.0 ) {
		NN1[kN1] = i;
		Ex1[kN1] = 0.0;
		Ey1[kN1] = 0.0;
		Ez1[kN1] = 0.0;
		px1[kN1] = 0.0;
		kN1++;
	}
	for (int i=0; i<N; i++) if (( coor[i].x>2.0 )&&(coor[i].x<4.0)) {
		NN2[kN2] = i;
		Ex2[kN2] = 0.0;
		Ey2[kN2] = 0.0;
		Ez2[kN2] = 0.0;
		px2[kN2] = 0.0;
		kN2++;
	}
	for (int i=0; i<N; i++) if (( coor[i].x>4.0 )&&(coor[i].x<5.6)) {
		NN3[kN3] = i;
		Ex3[kN3] = 0.0;
		Ey3[kN3] = 0.0;
		Ez3[kN3] = 0.0;
		px3[kN3] = 0.0;
		kN3++;
	}
/////////////////////////
	double px = 0.0;
	double py = 0.0;
	double pz = 0.0;
	double Ex = 0.0;
	double Ey = 0.0;
	double Ez = 0.0;

	double px_1 = 0.0;
	double py_1 = 0.0;
	double pz_1 = 0.0;
	double Ex_1 = 0.0;
	double Ey_1 = 0.0;
	double Ez_1 = 0.0;

	double px_2 = 0.0;
	double py_2 = 0.0;
	double pz_2 = 0.0;
	double Ex_2 = 0.0;
	double Ey_2 = 0.0;
	double Ez_2 = 0.0;

	double px_3 = 0.0;
	double py_3 = 0.0;
	double pz_3 = 0.0;
	double Ex_3 = 0.0;
	double Ey_3 = 0.0;
	double Ez_3 = 0.0;
/////////////////////////
	for (int j=0; j<10000; j++)
	{
		step(Nlist, list, mt, tm, im, lm, jm, km);
		///////////////
		for (int i=0; i<kN1; i++) {
			px_1 = px_1 + coor[ NN1[i] ].vx;
			py_1 = py_1 + coor[ NN1[i] ].vy;
			pz_1 = pz_1 + coor[ NN1[i] ].vz;
			Ex_1 = Ex_1 + coor[ NN1[i] ].vx*coor[ NN1[i] ].vx;
			Ey_1 = Ey_1 + coor[ NN1[i] ].vy*coor[ NN1[i] ].vy;
			Ez_1 = Ez_1 + coor[ NN1[i] ].vz*coor[ NN1[i] ].vz;

			px1[i] = px1[i] + coor[ NN1[i] ].vx;
			Ex1[i] = Ex1[i] + coor[ NN1[i] ].vx*coor[ NN1[i] ].vx;
			Ey1[i] = Ey1[i] + coor[ NN1[i] ].vy*coor[ NN1[i] ].vy;
			Ez1[i] = Ez1[i] + coor[ NN1[i] ].vz*coor[ NN1[i] ].vz;
		}
		/////////////////////
		for (int i=0; i<kN2; i++) {
			px_2 = px_2 + coor[ NN2[i] ].vx;
			py_2 = py_2 + coor[ NN2[i] ].vy;
			pz_2 = pz_2 + coor[ NN2[i] ].vz;
			Ex_2 = Ex_2 + coor[ NN2[i] ].vx*coor[ NN2[i] ].vx;
			Ey_2 = Ey_2 + coor[ NN2[i] ].vy*coor[ NN2[i] ].vy;
			Ez_2 = Ez_2 + coor[ NN2[i] ].vz*coor[ NN2[i] ].vz;

			px2[i] = px2[i] + coor[ NN2[i] ].vx;
			Ex2[i] = Ex2[i] + coor[ NN2[i] ].vx*coor[ NN2[i] ].vx;
			Ey2[i] = Ey2[i] + coor[ NN2[i] ].vy*coor[ NN2[i] ].vy;
			Ez2[i] = Ez2[i] + coor[ NN2[i] ].vz*coor[ NN2[i] ].vz;
		}
		///////////////
		for (int i=0; i<kN3; i++) {
			px_3 = px_3 + coor[ NN3[i] ].vx;
			py_3 = py_3 + coor[ NN3[i] ].vy;
			pz_3 = pz_3 + coor[ NN3[i] ].vz;
			Ex_3 = Ex_3 + coor[ NN3[i] ].vx*coor[ NN3[i] ].vx;

			Ey_3 = Ey_3 + coor[ NN3[i] ].vy*coor[ NN3[i] ].vy;
			Ez_3 = Ez_3 + coor[ NN3[i] ].vz*coor[ NN3[i] ].vz;

			px3[i] = px3[i] + coor[ NN3[i] ].vx;
			Ex3[i] = Ex3[i] + coor[ NN3[i] ].vx*coor[ NN3[i] ].vx;
			Ey3[i] = Ey3[i] + coor[ NN3[i] ].vy*coor[ NN3[i] ].vy;
			Ez3[i] = Ez3[i] + coor[ NN3[i] ].vz*coor[ NN3[i] ].vz;
		}
		////////////////
		for (int i=0; i<N; i++) {
			px = px + coor[i].vx;
			py = py + coor[i].vy;
			pz = pz + coor[i].vz;
			Ex = Ex + coor[i].vx*coor[i].vx;
			Ey = Ey + coor[i].vy*coor[i].vy;
			Ez = Ez + coor[i].vz*coor[i].vz;
		}

	}
//////////////////
	Ex_1 = Ex_1 / (kN1*10000.0);
	Ey_1 = Ey_1 / (kN1*10000.0);
	Ez_1 = Ez_1 / (kN1*10000.0);

	Ex_2 = Ex_2 / (kN2*10000.0);
	Ey_2 = Ey_2 / (kN2*10000.0);
	Ez_2 = Ez_2 / (kN2*10000.0);

	Ex_3 = Ex_3 / (kN3*10000.0);
	Ey_3 = Ey_3 / (kN3*10000.0);
	Ez_3 = Ez_3 / (kN3*10000.0);

	Ex = Ex / (N*10000.0);
	Ey = Ey / (N*10000.0);
	Ez = Ez / (N*10000.0);
/////////////
	FILE *pfile;
	pfile = fopen( "sys.txt" , "w+");
	fprintf(pfile, "<px> = %.15le  | <py> = %.15le  | <pz> = %.15le  |\n", px, py, pz);
	fprintf(pfile, "<Ex> = %.15le  | <Ey> = %.15le  | <Ez> = %.15le  |\n", Ex, Ey, Ez);
	fclose(pfile);
//////////////////////////////////////////////////////
	pfile = fopen( "gip1.txt" , "w+");
	fprintf(pfile, "<px> 1 sloya = %.15le \n", px_1);
	fprintf(pfile, "<py> 1 sloya = %.15le \n", py_1);
	fprintf(pfile, "<pz> 1 sloya = %.15le \n", pz_1);
	fprintf(pfile, "<Ex> 1 sloya = %.15le \n", Ex_1);
	fprintf(pfile, "<Ey> 1 sloya = %.15le \n", Ey_1);
	fprintf(pfile, "<Ez> 1 sloya = %.15le \n", Ez_1);
	fprintf(pfile, "----------------------------------------------------------------------------------------------\n");
    for (int i = 0; i < kN1; i++) {
        fprintf(pfile, "%.i  | %.15le  | %.15le  | %.15le  | %.15le  |\n", NN1[i], px1[i], Ex1[i]/10000, Ey1[i]/10000, Ez1[i]/10000);
		fprintf(pfile, "----------------------------------------------------------------------------------------------\n");
    }
	fclose(pfile);
///////////////////////////////////////////////////////
	pfile = fopen( "gip2.txt" , "w+");
	fprintf(pfile, "<px> 2 sloya = %.15le \n", px_2);
	fprintf(pfile, "<py> 2 sloya = %.15le \n", py_2);
	fprintf(pfile, "<pz> 2 sloya = %.15le \n", pz_2);
	fprintf(pfile, "<Ex> 2 sloya = %.15le \n", Ex_2);
	fprintf(pfile, "<Ey> 2 sloya = %.15le \n", Ey_2);
	fprintf(pfile, "<Ez> 2 sloya = %.15le \n", Ez_2);
	fprintf(pfile, "----------------------------------------------------------------------------------------------\n");
    for (int i = 0; i < kN2; i++) {
        fprintf(pfile, "%.i  | %.15le  | %.15le  | %.15le  | %.15le  |\n", NN2[i], px2[i], Ex2[i]/10000, Ey2[i]/10000, Ez2[i]/10000);
		fprintf(pfile, "----------------------------------------------------------------------------------------------\n");
    }
	fclose(pfile);
///////////////////////////////////////////////////////
	pfile = fopen( "gip3.txt" , "w+");
	fprintf(pfile, "<px> 3 sloya = %.15le \n", px_3);
	fprintf(pfile, "<py> 3 sloya = %.15le \n", py_3);
	fprintf(pfile, "<pz> 3 sloya = %.15le \n", pz_3);
	fprintf(pfile, "<Ex> 3 sloya = %.15le \n", Ex_3);
	fprintf(pfile, "<Ey> 3 sloya = %.15le \n", Ey_3);
	fprintf(pfile, "<Ez> 3 sloya = %.15le \n", Ez_3);
	fprintf(pfile, "----------------------------------------------------------------------------------------------\n");
    for (int i = 0; i < kN3; i++) {
        fprintf(pfile, "%.i  | %.15le  | %.15le  | %.15le  | %.15le  |\n", NN3[i], px3[i], Ex3[i]/10000, Ey3[i]/10000, Ez3[i]/10000);
		fprintf(pfile, "----------------------------------------------------------------------------------------------\n");
    }
	fclose(pfile);
///////////////////////////////////////////////////////
}

////////////////////////////////////////////////////////////////////////////////////////


void image_poloski(int &Nlist, int *list, int &mt, double *tm, int *im, int *lm, int *jm, int *km) {
	double IMG_X[1000][500];
	double IMG_Y[1000][500];
	int i, j, ii;

	for(i = 0; i < 1000; i++)

		for (j = 0; j < 500; j++)
			IMG_X[i][j] = IMG_Y[i][j] = 0.0;

	for (j = 0; j < 500; j++) {
		step(Nlist, list, mt, tm, im, lm, jm, km);
		i = 0;
		for (ii = 0; ii < N; ii++)
			if ( (coor[ii].x <= 41.0L) && (coor[ii].x >= 1.0L)
			&& (coor[ii].y[0] >= 17.50L) && (coor[ii].y[0] <= 22.5L)  ) {
				IMG_X[i][j] = coor[ii].x;
				IMG_Y[i][j] = coor[ii].y[0];
				i++; if (i == 999) continue;
			}
	}

	for (i = 0; i < 1000; i++)
		for (j = 0; j < 500; j++) {
			fprintf(image_profile_file,"%.15le\n", IMG_X[i][j] );

			fprintf(image_profile_file,"%.15le\n", IMG_Y[i][j] );
		}
}




//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

void save() {
	int i;
    fprintf(save_file, "%d\n", N);
    fprintf(save_file, "%.15le\n", A);

    fprintf(save_file, "%.15le\n", L);

    for (i = 0; i < N; i++) {
        fprintf(save_file, "%.15le %.15le %.15le %.15le \n", coor[i].x, coor[i].y[0], coor[i].z[0], coor[i].p);
        fprintf(save_file, "%.15le %.15le %.15le \n", coor[i].vx, coor[i].vy, coor[i].vz);
    }
}


//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////


void load(int &Nlist, int *list, int &mt, double *tm, int *im, int *lm, int *jm, int *km) {
	char S[40];
	double etta;
	int K = 0;
	int i, j, m;

	FILE *load_file = fopen("program.txt", "r");
	fscanf(load_file, "%i", &K);

	for (i = 0; i <= K; i++) {
		//clock_t tStart = clock();       //  засекаем время.

		fscanf(load_file, "%s", S);
		printf("|%s|\n", S);

		if ( (S[0] == 'n') && (S[1] == 'e') && (S[2] == 'w') &&
        (S[3] == '_') && (S[4] == 's') && (S[5] == 'e') &&
        (S[6] == 'e') && (S[7] == 'd') ) {
            fscanf(load_file, "%le\n", &etta);
			printf("start new seed\n");
            //new_seed(8, etta, 0.1L);
            printf("complite new seed\n");
            purgeX(Nlist, list, mt, tm, im, lm, jm, km);
	        retime(Nlist, list, mt, tm, im, lm, jm, km);
        } else
        if ( (S[0] == 'l') && (S[1] == 'o') && (S[2] == 'a') &&
        (S[3] == 'd') && (S[4] == '_') && (S[5] == 's') &&
        (S[6] == 'e') && (S[7] == 'e') && (S[8] == 'd') ) {
            printf("loading seed...\n");
            load_seed();
			printf("loading seed complite\n");
            purgeX(Nlist, list, mt, tm, im, lm, jm, km);
	        retime(Nlist, list, mt, tm, im, lm, jm, km);
        } else
        if ( (S[0] == 'c') && (S[1] == 'o') && (S[2] == 'm') &&
        (S[3] == 'p') && (S[4] == 'r') && (S[5] == 'e') &&
        (S[6] == 's') && (S[7] == 's') ) {
            fscanf(load_file, "%le\n", &etta);
			printf("start compress system...\n");
			compress( etta, Nlist, list, mt, tm, im, lm, jm, km );
			printf("complite compress system\n");
        } else
        if ( (S[0] == 's') && (S[1] == 't') && (S[2] == 'e') && (S[3] == 'p') ) {
            fscanf(load_file, "%i\n", &m);
            for (j = 0; j < m; j++) {
				step(Nlist, list, mt, tm, im, lm, jm, km);
			}
        } else
        if ( (S[0] == 's') && (S[1] == 'a') && (S[2] == 'v') && (S[3] == 'e') ) {
            fscanf(load_file, "%s", S);
			printf("save start\n");
	        save_file = fopen( S , "w+");
	        save();
	        fclose(save_file);
			printf("save complite\n");
        } else
        if ( (S[0] == 'i') && (S[1] == 'm') && (S[2] == 'a') &&
        (S[3] == 'g') && (S[4] == 'e') ) {
            fscanf(load_file, "%s", S);
			printf("image f(n)_X start\n");
            image_file = fopen( S , "w+" );
	        image(Nlist, list, mt, tm, im, lm, jm, km);
	        fclose(image_file);
			printf("image f(n)_X complite\n");
        }
		/*
        std::cout << "\n Work_Time = ";
	    if ( (clock() - tStart) / 3600000 > 0)
	    {
		    std::cout << ( clock() - tStart ) / 3600000 << "h ";
		    tStart = tStart + 3600000*( ( clock() - tStart ) / 3600000);
	    }
	    if ( (clock() - tStart) / 60000 > 0)
	    {
		    std::cout << ( clock() - tStart ) / 60000 << "m ";
		    tStart = tStart + 60000*( ( clock() - tStart ) / 60000);
	    }
	    std::cout << ( clock() - tStart ) / 1000 << "c \n";
		*/

		check(-100, mt, tm, im, lm, jm, km);
    }
}


//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////


int main() {
    int mt = 0;			// количество элементов в линейке времен
    double tm[N_MAX];	// создаем элементы линейки времен.
    int im[N_MAX];		//
    int lm[N_MAX];           //
    int jm[N_MAX];		//
    int km[N_MAX];		//
    int Nlist;		// количество элементов в послании к ретайму
    int list[N_MAX];		// элементы послания к ретайму

	error_file = fopen("error.txt", "w");
	impulse_file = fopen("impulse.txt", "w");




	check(1, mt, tm, im, lm, jm, km);

	load_seed();
	purgeX(Nlist, list, mt, tm, im, lm, jm, km);
	retime(Nlist, list, mt, tm, im, lm, jm, km);
	x1 = 70.00L;
	x2 = 71.80L;

//  	image_profile_file = fopen( "image_profile_12_051162_200000.txt" , "w+" );
	//image_profile(Nlist, list, mt, tm, im, lm, jm, km);
	//fclose(image_profile_file);
 /*   image_file = fopen( "image_file_12_050162_+15000.txt" , "w+" );
	image(Nlist, list, mt, tm, im, lm, jm, km);
	fclose(image_file);
*/
//моё
/*
long int j;
float nkm;
char strok[50];
	for (j = 24534; j < 100000; j++)
        { nkm=j;
        sprintf(strok, "%f", nkm);
		step(Nlist, list, mt, tm, im, lm, jm, km);
                save_file = fopen( strok , "w+");
	        save();
	        fclose(save_file);
            }
*/


    it0=it1=it2=it3=it4=it5=it6=it7=0;
    ifstream in0("0.txt");
    ifstream in1("1.txt");
    ifstream in2("2.txt");
    ifstream in3("3.txt");
    ifstream in4("4.txt");
    ifstream in5("5.txt");
    ifstream in6("6.txt");
    ifstream in7("7.txt");

    int st_r;
    while (in0.peek()!=EOF)
    { in0>>st_r;
      Num0[it0]=st_r;
      Num0_m[it0]=true;
      it0++;
    }
     while (in1.peek()!=EOF)
    { in1>>st_r;
      Num1[it1]=st_r;
      Num1_m[it1]=true;
      it1++;
    }
     while (in2.peek()!=EOF)
    { in2>>st_r;
      Num2[it2]=st_r;
      Num2_m[it2]=true;
      it2++;
    }
     while (in3.peek()!=EOF)
    { in3>>st_r;
      Num3[it3]=st_r;
      Num3_m[it3]=true;
      it3++;
    }
     while (in4.peek()!=EOF)
    { in4>>st_r;
      Num4[it4]=st_r;
      Num4_m[it4]=true;
      it4++;
    }
     while (in5.peek()!=EOF)
    { in5>>st_r;
      Num5[it5]=st_r;
      Num5_m[it5]=true;
      it5++;
    }
    while (in6.peek()!=EOF)
    { in6>>st_r;
      Num6[it6]=st_r;
      Num6_m[it6]=true;
      it6++;
    }
    while (in7.peek()!=EOF)
    { in7>>st_r;
      Num7[it7]=st_r;
      Num7_m[it7]=true;
      it7++;
    }


    in0.close();
    in1.close();
    in2.close();
    in3.close();
    in4.close();
    in5.close();
    in6.close();
    in7.close();

    for (int fix=0;fix<100;fix++ )
    step(Nlist, list, mt, tm, im, lm, jm, km);

    save_file=fopen( "0.txt" , "w+");
	for(int i0=0;i0<it0;i0++)	if (Num0_m[i0]==true) fprintf( save_file , "%d\n", Num0[i0]);
	fclose(save_file);

	save_file=fopen( "1.txt" , "w+");
	for(int i1=0;i1<it1;i1++)	if (Num1_m[i1]==true) fprintf( save_file , "%d\n", Num1[i1]);
    fclose(save_file);

    save_file=fopen( "2.txt" , "w+");
	for(int i2=0;i2<it2;i2++)	if (Num2_m[i2]==true) fprintf( save_file , "%d\n", Num2[i2]);
    fclose(save_file);

    save_file=fopen( "3.txt" , "w+");
	for(int i3=0;i3<it3;i3++)	if (Num3_m[i3]==true) fprintf( save_file , "%d\n", Num3[i3]);
    fclose(save_file);

    save_file=fopen( "4.txt" , "w+");
	for(int i4=0;i4<it4;i4++)	if (Num4_m[i4]==true) fprintf( save_file , "%d\n", Num4[i4]);
    fclose(save_file);

    save_file=fopen( "5.txt" , "w+");
	for(int i5=0;i5<it5;i5++)	if (Num5_m[i5]==true) fprintf( save_file , "%d\n", Num5[i5]);
    fclose(save_file);

    save_file=fopen( "6.txt" , "w+");
	for(int i6=0;i6<it6;i6++)	if (Num6_m[i6]==true) fprintf( save_file , "%d\n", Num6[i6]);
    fclose(save_file);

    save_file=fopen( "7.txt" , "w+");
	for(int i7=0;i7<it7;i7++)	if (Num7_m[i7]==true) fprintf( save_file , "%d\n", Num7[i7]);
    fclose(save_file);




    fclose(error_file);
    fclose(impulse_file);


	return 0;
}
