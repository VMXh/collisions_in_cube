/*
	������ 6 ��� 2011 ����.
	������ ���� 2016 ����
*/

#define _USE_MATH_DEFINES
#define _CRT_SECURE_NO_DEPRECATE

#include <math.h> // ����������
#include <stdlib.h> // ����������� ����������
#include <stdio.h> // ��� ������ � �����
#include <stdarg.h>
#include <string.h>
#include <assert.h>
#include <inttypes.h>

#include <algorithm>
#include <vector>
#include <fstream>
#include <mutex>
#include <queue>
#include <thread>

using namespace std;

#define N_MAX 7100

typedef struct _Mass_s {
	bool m[200];
	int Num[200];
	int it;
} Mass_s; // !!! ��� ����� �������?
Mass_s MassArray[8]; // !!! ��� ����� �������?

const double PI = 3.14159265358979;

int CR; // ������� ���������� ���������� � ��������� ������.
double Npress; // ����� ���� � ������, ��������������� ��� ��������� ��������.
double Time; // ����� ��������� ��������, � �������� ������� �������.
bool STOP = false; // ���� � ������� ��������� ����������� ����� ���������, �� ������������.

double x1 = 0.0;
double x2 = 0.0;

double cube; // ����������, ������� ���������� ��� "�������" ��� �������� ������������.

///

inline int fseek_64(FILE* _Stream, uint64_t _Offset, int _Origin) {
#ifdef _WIN32
	return _fseeki64(_Stream, _Offset, _Origin);
#else
	// 32 !!!
	return fseek(_Stream, _Offset, _Origin);
#endif
}

inline uint64_t ftell_64(FILE* _Stream) {
#ifdef _WIN32
	return _ftelli64(_Stream);
#else
	// 32 !!!
	return ftell(_Stream);
#endif
}

class LogFile {
private:
	FILE *fp;
	LogFile() {
		static const char* logfile_filename = "logfile.txt";
		fp = fopen(logfile_filename, "w+");
		if (!fp) throw "can't open logfile";
	}

public:
	static LogFile& getInstance() {
		static LogFile instance;
		return instance;
	}

	void printf(const char* format, ...) {
		va_list va;
		va_start(va, format);
		vfprintf(fp, format, va);
		va_end(va);
	}

	~LogFile() {
		fclose(fp);
	}
};

#define LOGFILE LogFile::getInstance()

// ��������� ��������� ������.
struct particle_t {
	// ���������� � ��� ������� � �� ������� ���������.
	// ���������� Y � Z ��� ������� � �� ������ ��������.
	double x;
	double y[4];
	double z[4];
	double vx, vy, vz;

	int NOBR; // NOBR - ���������� �������, ������� ����� �������.
	int OBR; // OBR - ����� ������, ���� �� ������������ (1 - �� Y, 2 - �� Z)		  
	double p; // p - ����������, ��� ������ ���������� ������� �� �������, ��������������� ��������� ������.
};

int N; // ���������� �������
double A, L; // ��������� ������. � - �������� �� � � z, L - �������� �� �.
particle_t* gParticles; // ��������� ���������

volatile bool gStopFlag = false; // true ���� �������� ������� �� ����������� ��������� (Ctrl + C � ��)

class SnapshotSerializer {
private:
	typedef uint64_t off64_t;
#pragma pack(push, 1)
	typedef struct {
		double x;
		double y;
		double z;
		double p;
		double vx;
		double vy;
		double vz;
	} compact_particle_t;
	typedef struct {
		int N;
		double A;
		double L;
	} element_hdr_t;
#pragma pack(pop)

	static const uint8_t EAT_flag_VALID = 1 << 0;

#pragma pack(push, 1)	
	typedef struct {
		uint8_t flags; // EAT_flag_*
		uint64_t timestamp;
		off64_t start_offset;
		uint32_t size;
	} EAT_element_t;
#pragma pack(pop)	

	// EAT = Element Allocation Table
	static const int EAT_max_elements_count = 5; // ������������ ���������� Element ���������� EAT ���� 1 ������ ���������������� ��� ��������� �� ��������� EAT
	static const off64_t EAT_size = sizeof(EAT_element_t)*EAT_max_elements_count;
	static const off64_t EAT_last_element_offset = sizeof(EAT_element_t)*(EAT_max_elements_count - 1);
	// 
	static uint8_t empty_EAT[EAT_size];
	//
	FILE* fp;
	//
	size_t EAT_free_element_index;
	off64_t free_element_offset;
	//
	vector<off64_t> EAT_list;
	//
#define CURRENT_EAT_BASE EAT_list.back()
	//
	void EAT_get_element(off64_t base, int idx, time_t* timestamp, off64_t* start_offset, uint32_t* size) {
		EAT_element_t EAT_element;
		off64_t saved_pos = ftell_64(fp);
		int result;
		result = fseek_64(fp, base + idx*sizeof(EAT_element_t), SEEK_SET);
		assert(result == 0);
		int readed = fread(&EAT_element, 1, sizeof(EAT_element), fp);
		assert(readed == sizeof(EAT_element));
		result = fseek_64(fp, saved_pos, SEEK_SET);
		assert(result == 0);
		assert((EAT_element.flags & EAT_flag_VALID) == EAT_flag_VALID);
		if (start_offset) *start_offset = EAT_element.start_offset;
		if (size) *size = EAT_element.size;
		if (timestamp) *timestamp = (time_t)EAT_element.timestamp;
	}

	void EAT_set_element(off64_t base, int idx, off64_t start_offset, uint32_t size) {
		time_t timestamp;
		EAT_element_t EAT_element;
		int result;
		//	
		time(&timestamp);
		memset(&EAT_element, 0, sizeof(EAT_element));
		//
		off64_t saved_pos = ftell_64(fp);
		result = fseek_64(fp, base + idx*sizeof(EAT_element_t), SEEK_SET);
		assert(result == 0);
		EAT_element.flags = EAT_flag_VALID;
		EAT_element.start_offset = start_offset;
		EAT_element.size = size;
		EAT_element.timestamp = (uint64_t)timestamp;
		int writted = fwrite(&EAT_element, 1, sizeof(EAT_element), fp);
		assert(writted == sizeof(EAT_element));
		result = fseek_64(fp, saved_pos, SEEK_SET);
		assert(result == 0);
	}

	void EAT_allocate_next_table() {
		EAT_set_element(CURRENT_EAT_BASE, EAT_max_elements_count - 1, free_element_offset, EAT_size);
		EAT_list.push_back(free_element_offset);
		EAT_free_element_index = 0;
		//
		int writted = fwrite(empty_EAT, 1, EAT_size, fp);
		assert(writted == EAT_size);
		free_element_offset = CURRENT_EAT_BASE + EAT_size;
		//
		assert(ftell_64(fp) == free_element_offset);
	}

	void EAT_check_overflow() {
		if (EAT_free_element_index == EAT_max_elements_count - 1)
			EAT_allocate_next_table();
	}

	void EAT_init_information() {
		size_t readed;
		int result;
		off64_t filesize;
		//
		result = fseek_64(fp, 0, SEEK_END);
		assert(result == 0);
		filesize = ftell_64(fp);
		if (filesize == 0)
			fwrite(empty_EAT, 1, EAT_size, fp);
		//
		EAT_list.push_back(0);
		for (;;) { // locate last EAT in file
			result = fseek_64(fp, CURRENT_EAT_BASE + EAT_last_element_offset, SEEK_SET);
			assert(result == 0);
			EAT_element_t EAT_element;
			readed = fread(&EAT_element, 1, sizeof(EAT_element), fp);
			if (readed != sizeof(EAT_element)) throw "StateSerializer: EAT element is truncated!";
			if ((EAT_element.flags & EAT_flag_VALID) != EAT_flag_VALID) break; // ������ EAT ���������, �� ��������� ������� (��������� �� ��������� �������) �� ���������������
			EAT_list.push_back(EAT_element.start_offset);
		}
		result = fseek_64(fp, CURRENT_EAT_BASE, SEEK_SET);
		assert(result == 0);
		EAT_element_t* pEAT = new EAT_element_t[EAT_max_elements_count];
		readed = fread(pEAT, 1, sizeof(*pEAT) * EAT_max_elements_count, fp);
		EAT_free_element_index = EAT_max_elements_count - 1;
		free_element_offset = CURRENT_EAT_BASE + EAT_size;
		for (int i = 0; i < EAT_max_elements_count - 1; i++) { // ���� ��������� �������������������� ������� EAT
			if ((pEAT[i].flags & EAT_flag_VALID) != EAT_flag_VALID) {
				EAT_free_element_index = i;
				break;
			}
			free_element_offset = pEAT[i].start_offset + pEAT[i].size;
		}
		result = fseek_64(fp, free_element_offset, SEEK_SET);
		assert(result == 0);
	}
public:
	typedef struct {
		off64_t base;
		uint32_t size;
		time_t timestamp;
	} snapshot_catalog_t;

	SnapshotSerializer(const char* fileName) {
		memset(empty_EAT, 0, EAT_size);
		fp = fopen(fileName, "rb+");
		if (!fp) fp = fopen(fileName, "wb+");
		if (!fp)
			throw "can't open snapshot file";
		EAT_init_information();
	}

	void add_snapshot(int N, double A, double L, particle_t* particles) {
		EAT_check_overflow();
		//
		uint32_t element_size = sizeof(element_hdr_t) + sizeof(compact_particle_t) * N;
		//
		uint8_t* pElement = new uint8_t[element_size];
		element_hdr_t* element_hdr = (element_hdr_t*)pElement;
		element_hdr->N = N;
		element_hdr->A = A;
		element_hdr->L = L;
		compact_particle_t* pParticles = (compact_particle_t*)(pElement + sizeof(element_hdr_t));
		for (int i = 0; i < N; i++) {
			pParticles[i].x = particles[i].x;
			pParticles[i].y = particles[i].y[0];
			pParticles[i].z = particles[i].z[0];
			pParticles[i].p = particles[i].p;
			pParticles[i].vx = particles[i].vx;
			pParticles[i].vy = particles[i].vy;
			pParticles[i].vz = particles[i].vz;
		}
		size_t writted = fwrite(pElement, 1, element_size, fp);
		assert(writted == element_size);
		fflush(fp);
		EAT_set_element(CURRENT_EAT_BASE, EAT_free_element_index, free_element_offset, element_size);
		free_element_offset += element_size;
		EAT_free_element_index++;
		delete[] pElement;
		//
		assert(ftell_64(fp) == free_element_offset);
	}

	int get_snapshot_count() {
		return (EAT_list.size() - 1) * (EAT_max_elements_count - 1) + EAT_free_element_index;
	}

	int get_catalog(snapshot_catalog_t*& catalog) {
		int snapshot_count = get_snapshot_count();
		catalog = new snapshot_catalog_t[snapshot_count + 1];
		int catalog_pos = 0;
		int result;
		EAT_element_t* pEAT = new EAT_element_t[EAT_max_elements_count];
		for (size_t i = 0; i < EAT_list.size(); i++) {
			off64_t saved_pos = ftell_64(fp);
			result = fseek_64(fp, EAT_list[i], SEEK_SET);
			assert(result == 0);
			int readed = fread(pEAT, 1, EAT_size, fp);
			assert(readed == EAT_size);
			result = fseek_64(fp, saved_pos, SEEK_SET);
			assert(result == 0);
			for (int j = 0; j < EAT_max_elements_count - 1; j++) {
				if ((pEAT[j].flags & EAT_flag_VALID) != EAT_flag_VALID)
					break;
				catalog[catalog_pos].base = pEAT[j].start_offset;
				catalog[catalog_pos].size = pEAT[j].size;
				catalog[catalog_pos].timestamp = (time_t)pEAT[j].timestamp;
				catalog_pos++;
			}
		}
		return snapshot_count;
	}

	void get_snapshot(int snapshot_idx, int &N, double &A, double &L, particle_t* &particles) {
		size_t target_EAT_index = snapshot_idx / (EAT_max_elements_count - 1);
		size_t target_EAT_element_index = snapshot_idx % (EAT_max_elements_count - 1);
		//
		assert(target_EAT_index < EAT_list.size());
		assert(!(target_EAT_index == EAT_list.size() && target_EAT_element_index >= EAT_free_element_index));
		off64_t target_EAT_base = EAT_list[target_EAT_index];
		//
		off64_t element_base;
		uint32_t element_size;
		EAT_get_element(target_EAT_base, target_EAT_element_index, NULL, &element_base, &element_size);
		//
		uint8_t* pElement = new uint8_t[element_size];
		//
		off64_t saved_pos = ftell_64(fp);
		int result = fseek_64(fp, element_base, SEEK_SET);
		assert(result == 0);
		int readed = fread(pElement, 1, element_size, fp);
		assert(readed == element_size);
		result = fseek_64(fp, saved_pos, SEEK_SET);
		assert(result == 0);
		//
		element_hdr_t* element_hdr = (element_hdr_t*)pElement;
		N = element_hdr->N;
		A = element_hdr->A;
		L = element_hdr->L;
		particles = new particle_t[N];
		//
		compact_particle_t* pParticles = (compact_particle_t*)(pElement + sizeof(element_hdr_t));
		for (int i = 0; i < N; i++) {
			particles[i].x = pParticles[i].x;
			particles[i].y[0] = pParticles[i].y;
			particles[i].z[0] = pParticles[i].z;
			particles[i].p = pParticles[i].p;
			particles[i].vx = pParticles[i].vx;
			particles[i].vy = pParticles[i].vy;
			particles[i].vz = pParticles[i].vz;
		}
		delete[] pElement;
	}

	void get_last_snapshot(int &N, double &A, double &L, particle_t* &particles) {
		assert(get_snapshot_count() > 0);
		get_snapshot(get_snapshot_count() - 1, N, A, L, particles);
	}

	~SnapshotSerializer() {
		fclose(fp);
	}
};

uint8_t SnapshotSerializer::empty_EAT[SnapshotSerializer::EAT_size];

void file_writer_worker();

class SnapshotManager {
private:
	typedef struct _task_t {
		int N;
		double A, L;
		particle_t* particles;
	} task_t;
	//
	SnapshotSerializer* SS;
	//
	bool file_writer_terminate;
	thread file_writer_thread;
	queue<task_t> task_queue;
	mutex task_queue_mutex;
	//
	SnapshotManager(const char* optional_name = NULL) {
		static const char* snapshot_filename = "snapshots.bin";
		SS = new SnapshotSerializer(optional_name == NULL ? snapshot_filename : optional_name);
		file_writer_terminate = false;
		file_writer_thread = thread(file_writer_worker);
	}
public:
	static SnapshotManager& getInstance() {
		static SnapshotManager instance;
		return instance;
	}

	void load_last() {
		// ��������� ��������� ������
		// ��������� ��������� ������
		printf("SnapshotManager: get_snapshot_count %i\n", SS->get_snapshot_count());
		if (SS->get_snapshot_count() == 0)
			throw "No saved snapshots!";
		SS->get_last_snapshot(N, A, L, gParticles);
		printf("SnapshotManager: Loaded N=%i A=%f L=%f\n", N, A, L);
		// ������� ������ ������
		for (int i = 0; i < N; i++) {
			// READY: gParticles[i].x
			// READY: gParticles[i].y[0]
			// READY: gParticles[i].z[0]
			// READY: gParticles[i].p
			if (gParticles[i].p <= L - 3.0) gParticles[i].p = L - 1.1; // !!! unused code !!!
			gParticles[i].p = L - 1.0; // ???
									   // READY: gParticles[i].vx
									   // READY: gParticles[i].vy
									   // READY: gParticles[i].vz
			gParticles[i].NOBR = 0;
			gParticles[i].y[1] = -10.0;
			gParticles[i].y[2] = -10.0;
			gParticles[i].y[3] = -10.0;
			if (gParticles[i].y[0] < 1.0) {
				gParticles[i].y[1] = gParticles[i].y[0] + A;
				gParticles[i].z[1] = gParticles[i].z[0];
				gParticles[i].NOBR = 1;
				gParticles[i].OBR = 1;
			} else if (gParticles[i].y[0] > A - 1.0) {
				gParticles[i].y[1] = gParticles[i].y[0] - A;
				gParticles[i].z[1] = gParticles[i].z[0];
				gParticles[i].NOBR = 1;
				gParticles[i].OBR = 1;
			}
			if (gParticles[i].z[0] < 1.0) {
				gParticles[i].z[2] = gParticles[i].z[0] + A;
				gParticles[i].y[2] = gParticles[i].y[0];
				if (gParticles[i].NOBR == 0) {
					gParticles[i].NOBR = 1;
					gParticles[i].OBR = 2;
				} else {
					gParticles[i].NOBR = 3;
				}
			} else if (gParticles[i].z[0] > A - 1.0) {
				gParticles[i].z[2] = gParticles[i].z[0] - A;
				gParticles[i].y[2] = gParticles[i].y[0];
				if (gParticles[i].NOBR == 0) {
					gParticles[i].NOBR = 1;
					gParticles[i].OBR = 2;
				} else {
					gParticles[i].NOBR = 3;
				}
			}
			if (gParticles[i].NOBR == 3) {
				gParticles[i].y[3] = gParticles[i].y[1];
				gParticles[i].z[3] = gParticles[i].z[2];
			}
		}
		double etta = (4.0 * PI * N) / (3.0 * A * A * (L - 2.0));
		cube = exp((1.0 / 3.0) * log(2.0 * PI / etta)); // �������� ����� ����, � ������� ����������� ������������
		cube = cube + 4.0;
		printf("SnapshotManager: pol_rebra = %f\n", cube);
		printf("SnapshotManager: etta = %f\n", etta);
	}

	void save_current() {
		task_t Task;
		Task.N = N;
		Task.A = A;
		Task.L = L;
		Task.particles = new particle_t[N];
		memcpy(Task.particles, gParticles, sizeof(particle_t)*N);
		task_queue_mutex.lock();
		task_queue.push(Task);
		task_queue_mutex.unlock();
	}

	void print_catalog() {
		SnapshotSerializer::snapshot_catalog_t* catalog;
		int count = SS->get_catalog(catalog);
		for (int i = 0; i < count; i++)
			printf("%i: base=%i timestamp=%.19s size=%i\n", i, (int)catalog[i].base, ctime(&catalog[i].timestamp), (int)catalog[i].size);
	}

	~SnapshotManager() {
		file_writer_terminate = true;
		file_writer_thread.join();
		delete SS;
	}

	friend void file_writer_worker();
};

#define SNAPSHOTS (SnapshotManager::getInstance())

void file_writer_worker() {
	SnapshotManager::task_t Task;
	while (true) {
		bool gotTask = false;
		SNAPSHOTS.task_queue_mutex.lock();
		if (!SNAPSHOTS.task_queue.empty()) {
			Task = SNAPSHOTS.task_queue.front();
			SNAPSHOTS.task_queue.pop();
			gotTask = true;
		}
		SNAPSHOTS.task_queue_mutex.unlock();
		if (gotTask) {
			//printf("file_writer_worker: save task\n");
			SNAPSHOTS.SS->add_snapshot(Task.N, Task.A, Task.L, Task.particles);
			delete[] Task.particles;
			//printf("file_writer_worker: save task complete\n");
		} else {
			if (SNAPSHOTS.file_writer_terminate) break;
		}
		this_thread::sleep_for(chrono::seconds(1));
	}
	//printf("file_writer_worker: exit\n");
}

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

void Del_mas(int nm_m) {
	if (gParticles[nm_m].NOBR > 0) {
		for (int i = 0; i < sizeof(MassArray) / sizeof(MassArray[0]); i++) {
			for (int j = 0; j<MassArray[i].it; j++)
				if (MassArray[i].Num[j] == nm_m) {
					MassArray[i].m[j] = false;
					break;
				}
		}
	}
}

/////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////

double Random() {
	return (double)rand() / RAND_MAX;
}

int Random(int LimitPlusOne) {
	return int(Random() * LimitPlusOne);
}

double RandG(double Mean, double StdDev) {
	// Marsaglia-Bray algorithm 
	double U1, S2;
	do {
		U1 = 2 * Random() - 1;
		S2 = pow(U1, 2) + pow(2 * Random() - 1, 2);
	} while (S2 >= 1);
	return sqrt(-2 * log(S2) / S2) * U1 * StdDev + Mean;
}

// ��������� ������ ������ �������.
// �������� � �������� ����������
// NN - ����� ������ � �����
// etta - ��������� ���������
// Srkv - ������� ������������ ��������.
void new_seed(int NN, double etta, double Srkv) {
	int KZ = 2;

	double axy, axz, v0x, v0y, v0z, v1x, v1y, v1z, v2x, v2y, v2z, v3x, v3y, v3z;

	double rb = 2.0 * sqrtl(2.0);      // ���������� ����� ����� ������
	double rbp = sqrtl(2.0);
	double A0 = rb*(NN - 1.0) + 2.0;         // ������� ���������� ���������� ������
	double L0 = rb*(2.0 * NN - 1.0) + 2.0;        //

	double mL0 = L0;

	double Betta = 0.0;

	double XC, YC, ZC; // ���������� ������ ������
	int NA;      // ���������� ��������� ������
	double ax, ay, az, vx, vy, vz;

	N = int(2.0*NN*(NN*NN + (NN - 1.0) * (NN - 1.0)));   //  ��������� ����� ������.
	N = N + int((2.0 * NN - 1.0) * 2.0 * (NN - 1.0) * NN);         //

	if (gParticles) delete[] gParticles;
	gParticles = new particle_t[N_MAX];

	double etta0 = (4.0 * M_PI * N) / (3.0 * A0 * A0 * (L0 - 1));   // ������� �������������� ���������.

	double Alpha = etta0 / etta;

	double sk = (1.0 / (Alpha*(L0 - 1.0))) * sqrtl((L0 * L0 / 4.0) + 1.0 / (27.0L * Alpha * (L0 - 1.0)));

	Betta = expl((1.0 / 3.0) * logl(L0 / (2.0 * Alpha * (L0 - 1.0)) + sk));
	Betta = Betta + expl((1.0 / 3.0) * logl(-L0 / (2.0 * Alpha * (L0 - 1.0)) - sk));

	Betta = 1.0 / Betta;

	XC = L0 / 2.0;    //
	YC = A0 / 2.0;    //  ���������� ������ ������.
	ZC = A0 / 2.0;    //

	L0 = L0*KZ;
	L = L0 * Betta;
	A = A0 * Betta;

	NA = 0;

	for (int i = 0; i < 2 * NN; i++)
		for (int j = 0; j < NN; j++)
			for (int k = 0; k < NN / 2; k++) {
				vx = RandG(0.0, Srkv);  //  ������ ��������.
				vy = RandG(0.0, Srkv);  //  ��� ����� ��������� ��� ���������� ������ �����(� ��������� �����������)
				vz = RandG(0.0, Srkv);  //  ����� ��� ����� ���� ������� � ������ �������� �������.

				for (int ii = -1; ii < 2; ii += 2) {
					if ((i == 0) && (ii>0)) continue;

					ax = XC + i*ii*rbp;

					for (int jj = -1; jj < 2; jj += 2) {
						if ((j == 0) && (jj>0)) continue;

						ay = YC + j*jj*rbp;

						for (int kk = -1; kk < 2; kk += 2) {
							if (((i % 2 == 0) && (j % 2 == 0)) || ((i % 2 != 0) && (j % 2 != 0)))
								az = ZC + kk*(rbp + k*rb);
							else {
								if ((k == 0) && (kk > 0)) continue;
								az = ZC + kk*k*rb;
							}

							gParticles[NA].x = ax;     	// ��������� ���������� ��� ����� ��������
							gParticles[NA].y[0] = ay;     // ��������� Betta - ��� ����������� ����������
							gParticles[NA].z[0] = az;     // �� ���� ���������� ��� ���������� � ��������� ������

							if ((j == 0) && (fabs(ZC - az)<0.1E-15L) && ((i / 2) * 2 != i))
								if (((i + 1) / 4) * 4 != (i + 1)) {
									axy = vy;
									axz = vz;
									if (ii>0) { vy = -vy; vz = -vz; }
									vy = ii*vy;
									vz = ii*vz;
								} else {
									vy = axy;
									vz = axz;
									vy = ii*vy;
									vz = ii*vz;
								}
							if (i == 0) {
								if ((j == 0) && (kk<0))
									switch (k) {
										case 0: v0x = vx; v0y = vy; v0z = vz; break;
										case 1: v1x = vx; v1y = vy; v1z = vz; break;
										case 2: v2x = vx; v2y = vy; v2z = vz; break;
										case 3: v3x = vx; v3y = vy; v3z = vz; break;
									} else
										switch (k) {
											case 0: vx = -v0x; vy = -v0y; vz = -v0z; break;
											case 1: vx = -v1x; vy = -v1y; vz = -v1z; break;
											case 2: vx = -v2x; vy = -v2y; vz = -v2z; break;
											case 3: vx = -v3x; vy = -v3y; vz = -v3z; break;
										}

									if ((k == 0) && ((j / 2) * 2 != j))
										switch (j) {
											case 1: vx = jj*v0x; vy = jj*v0y; vz = jj*v0z; break;
											case 3: vx = jj*v1x; vy = jj*v1y; vz = jj*v1z; break;
											case 5: vx = jj*v2x; vy = jj*v2y; vz = jj*v2z; break;
											case 7: vx = jj*v3x; vy = jj*v3y; vz = jj*v3z; break;
										}
							}

							gParticles[NA].vx = vx*ii*jj*kk;
							gParticles[NA].vy = vy*ii*jj*kk;
							gParticles[NA].vz = vz*ii*jj*kk;

							NA++;
						}
					}
				}
			}

	// �������� ������� ������� ���, ������� �����������.
	for (int jj = 1; jj < KZ; jj++) {
		for (int ii = 0; ii < N; ii++) {
			gParticles[ii + jj*N].x = gParticles[ii].x + jj*mL0;
			gParticles[ii + jj*N].y[0] = gParticles[ii].y[0];
			gParticles[ii + jj*N].z[0] = gParticles[ii].z[0];

			gParticles[ii + jj*N].vx = gParticles[ii].vx;
			gParticles[ii + jj*N].vy = gParticles[ii].vy;
			gParticles[ii + jj*N].vz = gParticles[ii].vz;
		}
	}

	N = N*KZ;

	// "���������" ������� �� ����������� ���������.
	for (int ii = 0; ii < N; ii++) {
		gParticles[ii].x = gParticles[ii].x*Betta;
		gParticles[ii].y[0] = gParticles[ii].y[0] * Betta;
		gParticles[ii].z[0] = gParticles[ii].z[0] * Betta;
	}

	// �������� ������ �����, ��� ��� ������ ����������.
	for (int ii = 0; ii < N; ii++)
		gParticles[ii].p = L - Random(9)*0.1L;

	/// ������ ��������� �������� ��� ��������� �������.
	for (int i = 0; i < N_MAX; i++) {
		gParticles[i].y[1] = -10.0;
		gParticles[i].y[2] = -10.0;
		gParticles[i].y[3] = -10.0;

		gParticles[i].z[1] = -10.0;
		gParticles[i].z[2] = -10.0;
		gParticles[i].z[3] = -10.0;

		gParticles[i].NOBR = 0;
	}


	// ������������ ����� ���� ��������������.
	cube = expl((1.0 / 3.0) * logl(2.0 * M_PI / etta));   // �������� ����� ����,
														  // � ������� ����������� ������������
	cube = cube + 2.0;

	// ������� ��������� ������� ����� ������.
	printf(" cube=%f;\n", cube);
	printf(" N=%i;  A=%f;  L=%f;\n", N, A, L);
	printf(" etta=%f;\n", (4.0 * M_PI * N) / (3.0 * A * A * (L - 1.0)));
}

// ��������� �������� ���������� ������� ��� ������� �� ��������(list)
// Nlist - ���������� ��������� � ��������
// list - �������� (�������� ������ �������, ��� ������� ���� ���������� ���������� �������)
// mt - ���������� ��������� � ������� ������
// tm,im,lm,jm,km - ������� ������.
void retime(int &Nlist, int *list, int &mt, double *tm, int *im, int *lm, int *jm, int *km) {
	static int i, ii, kk, jj, mm, mi, ml, mj, mk, sd;
	static double tmin, dt;
	static double ax, ay, az, avx, avy, avz;
	static double bx, by, bz, bvx, bvy, bvz;
	static double bij, dr, dv, Dis;

	for (sd = 0; sd < Nlist; sd++) {    // �� ����� ��������
										// ���������� ������ ������� � list[] ��� �������������� �� ���������� �������
		mi = list[sd];
		ml = 0; mk = -1;
		tmin = 1.0E+20;
		ax = gParticles[mi].x; ay = gParticles[mi].y[0]; az = gParticles[mi].z[0];
		avx = gParticles[mi].vx; avy = gParticles[mi].vy; avz = gParticles[mi].vz;

		// �������� ������� ��� ������
		if (avx < 0.0) {           /// X = 1
			dt = (1.0 - ax) / avx; if ((dt < tmin) && (dt > 0.0)) { tmin = dt; mj = 8; }
		} else {                   /// X = L-1
			dt = (L - 1.0 - ax) / avx; if ((dt < tmin) && (dt > 0.0)) { tmin = dt; mj = 9; }
			//dt = (gParticles[mi].p - ax) / avx; if ( (dt < tmin) && (dt > 0.0) ) { tmin = dt; mj = 10; }
		}

		if (avy > 0.0) {                       /// Y = A;
			dt = (A - ay) / avy; if ((dt < tmin) && (dt > 0.0)) { tmin = dt; mj = 0; }
			/// Y = A-1
			dt = (A - 1.0 - ay) / avy; if ((dt < tmin) && (dt > 0.0)) { tmin = dt; mj = 1; }
			/// Y = 1
			dt = (1.0 - ay) / avy; if ((dt < tmin) && (dt > 0.0)) { tmin = dt; mj = 3; }
		} else {                      /// Y = 0
			dt = -ay / avy; if ((dt < tmin) && (dt > 0.0)) { tmin = dt; mj = 2; }
			/// Y = 1
			dt = (1.0 - ay) / avy; if ((dt < tmin) && (dt > 0.0)) { tmin = dt; mj = 3; }
			/// Y = A-1
			dt = (A - 1.0 - ay) / avy; if ((dt < tmin) && (dt > 0.0)) { tmin = dt; mj = 1; }
		}

		if (avz > 0.0) {                   /// Z = A
			dt = (A - az) / avz; if ((dt < tmin) && (dt > 0.0)) { tmin = dt; mj = 4; }
			/// Z = A-1
			dt = (A - 1.0 - az) / avz; if ((dt < tmin) && (dt > 0.0)) { tmin = dt; mj = 5; }
			/// Z = 1
			dt = (1.0 - az) / avz; if ((dt < tmin) && (dt > 0.0)) { tmin = dt; mj = 7; }
		} else {                /// Z = 0
			dt = -az / avz; if ((dt < tmin) && (dt > 0.0)) { tmin = dt; mj = 6; }
			/// Z = 1
			dt = (1.0 - az) / avz; if ((dt < tmin) && (dt > 0.0)) { tmin = dt; mj = 7; }
			/// Z = A-1
			dt = (A - 1.0 - az) / avz; if ((dt < tmin) && (dt > 0.0)) { tmin = dt; mj = 5; }
		}

		////////////////////////////////
		////// ������� ������������
		////// ��������-��������
		////// ��������-�����
		////// �����-�����
		////////////////////////////////
		if (gParticles[mi].NOBR == 0) {	// ���� � ����� ������� ��� �������(���� ����� ����������� ��� �������)
									// �� ������������� ������ ���������� ������� � ������� ��������� � � �������� ������ ������

			for (i = 0; i < mi; i++) {	// ������������ � ������ ����� ������� �� 1 �� mi, � � ��������� �� mi+1 �� N
				bx = gParticles[i].x - ax;
				if (fabs(bx) > cube) continue;
				if (gParticles[i].NOBR == 0) {
					by = gParticles[i].y[0] - ay;
					if (fabs(by) < cube) {
						bz = gParticles[i].z[0] - az;
						if (fabs(bz) < cube) {
							bvx = gParticles[i].vx - avx;
							bvy = gParticles[i].vy - avy;
							bvz = gParticles[i].vz - avz;
							bij = bx*bvx + by*bvy + bz*bvz;
							if (bij < 0.0) {
								dr = bx*bx + by*by + bz*bz;
								dv = bvx*bvx + bvy*bvy + bvz*bvz;
								Dis = bij*bij + dv*(4.0 - dr);
								if (Dis > 0.0) {
									dt = (-bij - sqrt(Dis)) / dv;
									if ((dt < tmin) && (dt > 0.0)) {
										tmin = dt;
										ml = 0; mj = i; mk = 0;
									}
								}
							}
						}
					}
				} //end if
				else if (gParticles[i].NOBR == 1) {	// ���� � ������� ���� �����
												// �� ���������� ����������� ���������� ��� ����� ������� � �� ������
					kk = 0;	// kk ������� ����� 0 (���� �������), � ����� ������ ������
					for (ii = 0; ii < 2; ii++) {
						by = gParticles[i].y[kk] - ay;
						if (fabs(by) < cube) {
							bz = gParticles[i].z[kk] - az;
							if (fabs(bz) < cube) {
								bvx = gParticles[i].vx - avx;
								bvy = gParticles[i].vy - avy;
								bvz = gParticles[i].vz - avz;
								bij = bx*bvx + by*bvy + bz*bvz;
								if (bij < 0.0) {
									dr = bx*bx + by*by + bz*bz;
									dv = bvx*bvx + bvy*bvy + bvz*bvz;
									Dis = bij*bij + dv*(4.0 - dr);
									if (Dis > 0.0) {
										dt = (-bij - sqrt(Dis)) / dv;
										if ((dt < tmin) && (dt > 0.0)) {
											tmin = dt;
											ml = 0; mj = i; mk = kk;
										}
									}
								}
							}
						}
						kk = gParticles[i].OBR;
					}	// end for
				}	// end if
				else {	// ���� � ������� ��� ������, �� ���� ����������� ��� ��������� ����������
					for (ii = 0; ii < 4; ii++) {
						by = gParticles[i].y[ii] - ay;
						if (fabs(by) < cube) {
							bz = gParticles[i].z[ii] - az;
							if (fabs(bz) < cube) {
								bvx = gParticles[i].vx - avx;
								bvy = gParticles[i].vy - avy;
								bvz = gParticles[i].vz - avz;
								bij = bx*bvx + by*bvy + bz*bvz;
								if (bij < 0.0) {
									dr = bx*bx + by*by + bz*bz;
									dv = bvx*bvx + bvy*bvy + bvz*bvz;
									Dis = bij*bij + dv*(4.0 - dr);
									if (Dis > 0.0) {
										dt = (-bij - sqrt(Dis)) / dv;
										if ((dt < tmin) && (dt > 0.0)) {
											tmin = dt;
											ml = 0; mj = i; mk = ii;
										}
									}
								}
							}
						}
					}	// end for
				}
			}	// ����� ����� for (int i = 0; i < mi; i++)

				// ��������� �� ��� ����� �������� �������� �������� ������� ���������.
			for (i = mi + 1; i < N; i++) {
				bx = gParticles[i].x - ax;
				if (fabs(bx) > cube) continue;
				if (gParticles[i].NOBR == 0) {
					by = gParticles[i].y[0] - ay;
					if (fabs(by) < cube) {
						bz = gParticles[i].z[0] - az;
						if (fabs(bz) < cube) {
							bvx = gParticles[i].vx - avx;
							bvy = gParticles[i].vy - avy;
							bvz = gParticles[i].vz - avz;
							bij = bx*bvx + by*bvy + bz*bvz;
							if (bij < 0.0) {
								dr = bx*bx + by*by + bz*bz;
								dv = bvx*bvx + bvy*bvy + bvz*bvz;
								Dis = bij*bij + dv*(4.0 - dr);
								if (Dis > 0.0) {
									dt = (-bij - sqrt(Dis)) / dv;
									if ((dt < tmin) && (dt > 0.0)) {
										tmin = dt;
										ml = 0; mj = i; mk = 0;
									}
								}
							}
						}
					}
				} //end if
				else if (gParticles[i].NOBR == 1) {
					kk = 0;
					for (ii = 0; ii < 2; ii++) {
						by = gParticles[i].y[kk] - ay;
						if (fabs(by) < cube) {
							bz = gParticles[i].z[kk] - az;
							if (fabs(bz) < cube) {
								bvx = gParticles[i].vx - avx;
								bvy = gParticles[i].vy - avy;
								bvz = gParticles[i].vz - avz;
								bij = bx*bvx + by*bvy + bz*bvz;
								if (bij < 0.0) {
									dr = bx*bx + by*by + bz*bz;
									dv = bvx*bvx + bvy*bvy + bvz*bvz;
									Dis = bij*bij + dv*(4.0 - dr);
									if (Dis > 0.0) {
										dt = (-bij - sqrt(Dis)) / dv;
										if ((dt < tmin) && (dt > 0.0)) {
											tmin = dt;
											ml = 0; mj = i; mk = kk;
										}
									}
								}
							}
						}
						kk = gParticles[i].OBR;
					}	// end for
				}	// end if
				else {
					for (ii = 0; ii < 4; ii++) {
						by = gParticles[i].y[ii] - ay;
						if (fabs(by) < cube) {
							bz = gParticles[i].z[ii] - az;
							if (fabs(bz) < cube) {
								bvx = gParticles[i].vx - avx;
								bvy = gParticles[i].vy - avy;
								bvz = gParticles[i].vz - avz;
								bij = bx*bvx + by*bvy + bz*bvz;
								if (bij < 0.0) {
									dr = bx*bx + by*by + bz*bz;
									dv = bvx*bvx + bvy*bvy + bvz*bvz;
									Dis = bij*bij + dv*(4.0 - dr);
									if (Dis > 0.0) {
										dt = (-bij - sqrt(Dis)) / dv;
										if ((dt < tmin) && (dt > 0.0)) {
											tmin = dt;
											ml = 0; mj = i; mk = ii;
										}
									}
								}
							}
						}
					}	// end for
				}
			}	// ����� ����� for (int i = mi+1; i < N; i++)
		}	// end if
		else if (gParticles[mi].NOBR == 1) {	// ���� � ������� 1 ����� (����� ������ � 1 �� 7 �������)
			mm = 0;
			for (jj = 0; jj < 2; jj++) {
				for (i = 0; i < mi; i++) {
					bx = gParticles[i].x - ax;
					if (fabs(bx) > cube) continue;
					if (gParticles[i].NOBR == 0) {
						by = gParticles[i].y[0] - gParticles[mi].y[mm];
						if (fabs(by) < cube) {
							bz = gParticles[i].z[0] - gParticles[mi].z[mm];
							if (fabs(bz) < cube) {
								bvx = gParticles[i].vx - avx;
								bvy = gParticles[i].vy - avy;
								bvz = gParticles[i].vz - avz;
								bij = bx*bvx + by*bvy + bz*bvz;
								if (bij < 0.0) {
									dr = bx*bx + by*by + bz*bz;
									dv = bvx*bvx + bvy*bvy + bvz*bvz;
									Dis = bij*bij + dv*(4.0 - dr);
									if (Dis > 0.0) {
										dt = (-bij - sqrt(Dis)) / dv;
										if ((dt < tmin) && (dt > 0.0)) {
											tmin = dt;
											ml = mm; mj = i; mk = 0;
										}
									}
								}
							}
						}
					} //end if
					else if (gParticles[i].NOBR == 1) {
						kk = 0;
						for (ii = 0; ii < 2; ii++) {
							by = gParticles[i].y[kk] - gParticles[mi].y[mm];
							if (fabs(by) < cube) {
								bz = gParticles[i].z[kk] - gParticles[mi].z[mm];
								if (fabs(bz) < cube) {
									bvx = gParticles[i].vx - avx;
									bvy = gParticles[i].vy - avy;
									bvz = gParticles[i].vz - avz;
									bij = bx*bvx + by*bvy + bz*bvz;
									if (bij < 0.0) {
										dr = bx*bx + by*by + bz*bz;
										dv = bvx*bvx + bvy*bvy + bvz*bvz;
										Dis = bij*bij + dv*(4.0 - dr);
										if (Dis > 0.0) {
											dt = (-bij - sqrt(Dis)) / dv;
											if ((dt < tmin) && (dt > 0.0)) {
												tmin = dt;
												ml = mm;
												mj = i;
												mk = kk;
											}
										}
									}
								}
							}
							kk = gParticles[i].OBR;
						}	// end for
					}	// end if
					else {
						for (ii = 0; ii < 4; ii++) {
							by = gParticles[i].y[ii] - gParticles[mi].y[mm];
							if (fabs(by) < cube) {
								bz = gParticles[i].z[ii] - gParticles[mi].z[mm];
								if (fabs(bz) < cube) {
									bvx = gParticles[i].vx - avx;
									bvy = gParticles[i].vy - avy;
									bvz = gParticles[i].vz - avz;
									bij = bx*bvx + by*bvy + bz*bvz;
									if (bij < 0.0) {
										dr = bx*bx + by*by + bz*bz;
										dv = bvx*bvx + bvy*bvy + bvz*bvz;
										Dis = bij*bij + dv*(4.0 - dr);
										if (Dis > 0.0) {
											dt = (-bij - sqrt(Dis)) / dv;
											if ((dt < tmin) && (dt > 0.0)) {
												tmin = dt;
												ml = mm; mj = i; mk = ii;
											}
										}
									}
								}
							}
						}	// end for
					}
				}	// ����� ����� for (int i = 0; i < mi; i++)
				for (i = mi + 1; i < N; i++) {
					bx = gParticles[i].x - ax;
					if (fabs(bx) > cube) continue;
					if (gParticles[i].NOBR == 0) {
						by = gParticles[i].y[0] - gParticles[mi].y[mm];
						if (fabs(by) < cube) {
							bz = gParticles[i].z[0] - gParticles[mi].z[mm];
							if (fabs(bz) < cube) {
								bvx = gParticles[i].vx - avx;
								bvy = gParticles[i].vy - avy;
								bvz = gParticles[i].vz - avz;
								bij = bx*bvx + by*bvy + bz*bvz;
								if (bij < 0.0) {
									dr = bx*bx + by*by + bz*bz;
									dv = bvx*bvx + bvy*bvy + bvz*bvz;
									Dis = bij*bij + dv*(4.0 - dr);
									if (Dis > 0.0) {
										dt = (-bij - sqrt(Dis)) / dv;
										if ((dt < tmin) && (dt > 0.0)) {
											tmin = dt;
											ml = mm; mj = i; mk = 0;
										}
									}
								}
							}
						}
					} //end if
					else if (gParticles[i].NOBR == 1) {
						kk = 0;
						for (ii = 0; ii < 2; ii++) {
							by = gParticles[i].y[kk] - gParticles[mi].y[mm];
							if (fabs(by) < cube) {
								bz = gParticles[i].z[kk] - gParticles[mi].z[mm];
								if (fabs(bz) < cube) {
									bvx = gParticles[i].vx - avx;
									bvy = gParticles[i].vy - avy;
									bvz = gParticles[i].vz - avz;
									bij = bx*bvx + by*bvy + bz*bvz;
									if (bij < 0.0) {
										dr = bx*bx + by*by + bz*bz;
										dv = bvx*bvx + bvy*bvy + bvz*bvz;
										Dis = bij*bij + dv*(4.0 - dr);
										if (Dis > 0.0) {
											dt = (-bij - sqrt(Dis)) / dv;
											if ((dt < tmin) && (dt > 0.0)) {
												tmin = dt;
												ml = mm; mj = i; mk = kk;
											}
										}
									}
								}
							}
							kk = gParticles[i].OBR;
						}	// end for
					}	// end if
					else {
						for (ii = 0; ii < 4; ii++) {
							by = gParticles[i].y[ii] - gParticles[mi].y[mm];
							if (fabs(by) < cube) {
								bz = gParticles[i].z[ii] - gParticles[mi].z[mm];
								if (fabs(bz) < cube) {
									bvx = gParticles[i].vx - avx;
									bvy = gParticles[i].vy - avy;
									bvz = gParticles[i].vz - avz;
									bij = bx*bvx + by*bvy + bz*bvz;
									if (bij < 0.0) {
										dr = bx*bx + by*by + bz*bz;
										dv = bvx*bvx + bvy*bvy + bvz*bvz;
										Dis = bij*bij + dv*(4.0 - dr);
										if (Dis > 0.0) {
											dt = (-bij - sqrt(Dis)) / dv;
											if ((dt < tmin) && (dt > 0.0)) {
												tmin = dt;
												ml = mm; mj = i; mk = ii;
											}
										}
									}
								}
							}
						}	// end for
					}
				}	// ����� ����� for (int i = mi+1; i < N; i++)
				mm = gParticles[mi].OBR;
			}	// end for -> jj;
		}	// end if
		else {	// ���� � ������� 3 ������ (����� ������ � 3 �� 700 �������)
			for (jj = 0; jj < 4; jj++) {
				for (i = 0; i < mi; i++) {
					bx = gParticles[i].x - ax;
					if (fabs(bx) > cube) continue;
					if (gParticles[i].NOBR == 0) {
						by = gParticles[i].y[0] - gParticles[mi].y[jj];
						if (fabs(by) < cube) {
							bz = gParticles[i].z[0] - gParticles[mi].z[jj];
							if (fabs(bz) < cube) {
								bvx = gParticles[i].vx - avx;
								bvy = gParticles[i].vy - avy;
								bvz = gParticles[i].vz - avz;
								bij = bx*bvx + by*bvy + bz*bvz;
								if (bij < 0.0) {
									dr = bx*bx + by*by + bz*bz;
									dv = bvx*bvx + bvy*bvy + bvz*bvz;
									Dis = bij*bij + dv*(4.0 - dr);
									if (Dis > 0.0) {
										dt = (-bij - sqrt(Dis)) / dv;
										if ((dt < tmin) && (dt > 0.0)) {
											tmin = dt;
											ml = jj; mj = i; mk = 0;
										}
									}
								}
							}
						}
					} //end if
					else if (gParticles[i].NOBR == 1) {
						kk = 0;
						for (ii = 0; ii < 2; ii++) {
							by = gParticles[i].y[kk] - gParticles[mi].y[jj];
							if (fabs(by) < cube) {
								bz = gParticles[i].z[kk] - gParticles[mi].z[jj];
								if (fabs(bz) < cube) {
									bvx = gParticles[i].vx - avx;
									bvy = gParticles[i].vy - avy;
									bvz = gParticles[i].vz - avz;
									bij = bx*bvx + by*bvy + bz*bvz;
									if (bij < 0.0) {
										dr = bx*bx + by*by + bz*bz;
										dv = bvx*bvx + bvy*bvy + bvz*bvz;
										Dis = bij*bij + dv*(4.0 - dr);
										if (Dis > 0.0) {
											dt = (-bij - sqrt(Dis)) / dv;
											if ((dt < tmin) && (dt > 0.0)) {
												tmin = dt;
												ml = jj; mj = i; mk = kk;
											}
										}
									}
								}
							}
							kk = gParticles[i].OBR;
						}	// end for
					}	// end if
					else {
						for (ii = 0; ii < 4; ii++) {
							by = gParticles[i].y[ii] - gParticles[mi].y[jj];
							if (fabs(by) < cube) {
								bz = gParticles[i].z[ii] - gParticles[mi].z[jj];
								if (fabs(bz) < cube) {
									bvx = gParticles[i].vx - avx;
									bvy = gParticles[i].vy - avy;
									bvz = gParticles[i].vz - avz;
									bij = bx*bvx + by*bvy + bz*bvz;
									if (bij < 0.0) {
										dr = bx*bx + by*by + bz*bz;
										dv = bvx*bvx + bvy*bvy + bvz*bvz;
										Dis = bij*bij + dv*(4.0 - dr);
										if (Dis > 0.0) {
											dt = (-bij - sqrt(Dis)) / dv;
											if ((dt < tmin) && (dt > 0.0)) {
												tmin = dt;
												ml = jj; mj = i; mk = ii;
											}
										}
									}
								}
							}
						}	// end for
					}
				}	// ����� ����� for (int i = 0; i < mi; i++)
				for (i = mi + 1; i < N; i++) {
					bx = gParticles[i].x - ax;
					if (fabs(bx) > cube) continue;
					if (gParticles[i].NOBR == 0) {
						by = gParticles[i].y[0] - gParticles[mi].y[jj];
						if (fabs(by) < cube) {
							bz = gParticles[i].z[0] - gParticles[mi].z[jj];
							if (fabs(bz) < cube) {
								bvx = gParticles[i].vx - avx;
								bvy = gParticles[i].vy - avy;
								bvz = gParticles[i].vz - avz;
								bij = bx*bvx + by*bvy + bz*bvz;
								if (bij < 0.0) {
									dr = bx*bx + by*by + bz*bz;
									dv = bvx*bvx + bvy*bvy + bvz*bvz;
									Dis = bij*bij + dv*(4.0 - dr);
									if (Dis > 0.0) {
										dt = (-bij - sqrt(Dis)) / dv;
										if ((dt < tmin) && (dt > 0.0)) {
											tmin = dt;
											ml = jj; mj = i; mk = 0;
										}
									}
								}
							}
						}
					} //end if
					else if (gParticles[i].NOBR == 1) {
						kk = 0;
						for (ii = 0; ii < 2; ii++) {
							by = gParticles[i].y[kk] - gParticles[mi].y[jj];
							if (fabs(by) < cube) {
								bz = gParticles[i].z[kk] - gParticles[mi].z[jj];
								if (fabs(bz) < cube) {
									bvx = gParticles[i].vx - avx;
									bvy = gParticles[i].vy - avy;
									bvz = gParticles[i].vz - avz;
									bij = bx*bvx + by*bvy + bz*bvz;
									if (bij < 0.0) {
										dr = bx*bx + by*by + bz*bz;
										dv = bvx*bvx + bvy*bvy + bvz*bvz;
										Dis = bij*bij + dv*(4.0 - dr);
										if (Dis > 0.0) {
											dt = (-bij - sqrt(Dis)) / dv;
											if ((dt < tmin) && (dt > 0.0)) {
												tmin = dt;
												ml = jj; mj = i; mk = kk;
											}
										}
									}
								}
							}
							kk = gParticles[i].OBR;
						}	// end for
					}	// end if
					else {
						for (ii = 0; ii < 4; ii++) {
							by = gParticles[i].y[ii] - gParticles[mi].y[jj];
							if (fabs(by) < cube) {
								bz = gParticles[i].z[ii] - gParticles[mi].z[jj];
								if (fabs(bz) < cube) {
									bvx = gParticles[i].vx - avx;
									bvy = gParticles[i].vy - avy;
									bvz = gParticles[i].vz - avz;
									bij = bx*bvx + by*bvy + bz*bvz;
									if (bij < 0.0) {
										dr = bx*bx + by*by + bz*bz;
										dv = bvx*bvx + bvy*bvy + bvz*bvz;
										Dis = bij*bij + dv*(4.0 - dr);
										if (Dis > 0.0) {
											dt = (-bij - sqrt(Dis)) / dv;
											if ((dt < tmin) && (dt > 0.0)) {
												tmin = dt;
												ml = jj; mj = i; mk = ii;
											}
										}
									}
								}
							}
						}	// end for
					}
				}	// ����� ����� for (int i = mi+1; i < N; i++)
			}	// end for -> jj;
		}	// end else()

			/////////////////////////////////// ������� ������ ������� � ������� ������
		for (i = 0; i <= mt; i++)
			if (tmin < tm[i]) { // ������� ����� � ������� ������ (���������� �������� �������?)
								// �������� "������ �����"
								// ���� �� �����, �� ����� ������� �� ����������� � ������� ������
				if ((i > 0) && (
					((im[i - 1] == mj) && (jm[i - 1] == mi) && (km[i - 1] == ml) && (lm[i - 1] == mk)) ||
					((im[i - 1] == mi) && (jm[i - 1] == mj) && (km[i - 1] == mk) && (lm[i - 1] == ml))))
					break;

				// ������� �������
				// ����������� ��� ��������, ������� � i-����, ������
				for (ii = mt, kk = mt - 1; ii > i; ii--, kk--) {
					tm[ii] = tm[kk];
					im[ii] = im[kk]; lm[ii] = lm[kk];
					jm[ii] = jm[kk]; km[ii] = km[kk];
				}
				mt++;
				tm[mt] = 1.0E+20;

				// ������� ������ �������
				tm[i] = tmin;
				im[i] = mi; lm[i] = ml;
				jm[i] = mj; km[i] = mk;

				break;
			}
	}
	// ����� ��������� ���� ������ �� list[] �������� ����� list[]
	Nlist = 0;
}


/////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////

// ��������� ������� ������� ������ �� ���������� ������������� �������.
// � �������� ���������� ��������:
// Nlist - ���������� ��������� ��������( 1 ��� 2 )
// list - �������� (������ �������, � �������� ��������� �������)
// ��� ��������� �� �������� ���� ��������� ������� ������
// � ��������� ���� ������������ ��������� �������.
// mt - ���������� ��������� � ������� ������
// tm,im,lm,jm,km - ������� ������
void purge(int &Nlist, int *list, int &mt, double *tm, int *im, int *lm, int *jm, int *km) {
	static int m, i, j, p, k;
	m = Nlist;		// ���������� �������������� ���������� ��������� � ��������
	for (i = 0; i < m; i++) {          // ��� ������� �������� ��������������� ��������
		j = 0;	// ����� ���������������� �������
		while (j < mt) {               // ������ ������� ������
									   // ������������� ������ im[] �� ������� ������, ��� ������� ��������� �������
			if (im[j] == list[i]) {
				if (km[j] >= 0) {	// ���� � ���������������� ������� ��������� ������ �������
									// �� �������� �� � list[] ��� ��������� ����������� ������� ���������� �������
					list[Nlist] = jm[j];
					Nlist++;
				}
				// ������� ��������� ���������������� ������� �� ������� ������
				for (p = j, k = j + 1; p < mt; p++, k++) {
					tm[p] = tm[k];
					im[p] = im[k]; lm[p] = lm[k];
					jm[p] = jm[k]; km[p] = km[k];
				}
				mt--;
				tm[mt] = 1.0E+20;
				continue;
			}
			// ������������� ������ jm[] �� ������� ������, ��� ������� ��������� �������
			if ((jm[j] == list[i]) && (km[j] >= 0)) {
				// ��������� ������ �������, ����������� � ���������������� ������� � list[] ���
				// ��������� ����������� ������� ���������� �������
				list[Nlist] = im[j];
				Nlist++;
				// ������� ��������� ���������������� ������� �� ������� ������
				for (p = j, k = j + 1; p < mt; p++, k++) {
					tm[p] = tm[k];
					im[p] = im[k]; lm[p] = lm[k];
					jm[p] = jm[k]; km[p] = km[k];
				}
				mt--;
				tm[mt] = 1.0E+20;
				continue;
			}
			// ���� �� ������� ������� �� ������� ������, �� ��������� � ����������
			// ���� �������, �� ����� ���������� ��������� � ������� ���������� �
			// ����������� j �� �����
			j++;
		}
	}
}


////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////

// ��������� ���������� �������.
// ��������� �������� ������� ������
// ��������� �������� � ������� �������� ���� �������
// ��� �������� �� ���������� �������.
void purgeX(int &Nlist, int *list, int &mt, double *tm, int *im, int *lm, int *jm, int *km) {
	int i;
	for (i = 0; i < N_MAX; i++) { 
		list[i] = 0; 
		tm[i] = 1.0E+20; 
	}
	for (i = 0; i < N; i++) list[i] = i;
	Nlist = N;
	mt = 0;
}

////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////

// ��������� ������������ ��������.
// �������� ���������� �������, ����� ���������� ���������
// �� ������� 0 ��� �.
// ���� � �������� ���� ������ ������, �� ��� �������� �������.
// mi - ����� ��������
// mj - ����� ������ �� ������� ��������� ������������
void reName(int mi, int mj) {
	static double k;
	if (mj < 4) {    // ���� mj<4 - �� ��� ����� �� Y
		k = gParticles[mi].y[0];				// ���������� ���������� ������
		gParticles[mi].y[0] = gParticles[mi].y[1];	// ���������� ������ ������������ ���������� ��������
		gParticles[mi].y[1] = k;				// ������ ����� ���������� ��������
		gParticles[mi].y[2] = gParticles[mi].y[0];	// ������ ���������� ��� 2��
		gParticles[mi].y[3] = gParticles[mi].y[1];	// � 3�� ������
	} else {        // ����� ��� ����� �� Z
		k = gParticles[mi].z[0];				// ���������� ���������� ������
		gParticles[mi].z[0] = gParticles[mi].z[2];	// ���������� ������ ������������ ���������� ��������
		gParticles[mi].z[2] = k;				// ������ ����� ���������� ��������
		gParticles[mi].z[1] = gParticles[mi].z[0];	// ������ ���������� ��� 1��
		gParticles[mi].z[3] = gParticles[mi].z[2];	// � 3�� ������
	}
}

////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////

// ��������� �������� ��������� ������� ����� ������������� ������������
// �������� � �������� ����������:
// mi - ����� ������ �� ������������� �������
// ml - ����� ������ ������ ��������
// mj - ����� ������ �� ������������� �������
// mk - ����� ������ ������ ��������
void collision(int mi, int ml, int mj, int mk) {
	static double v1x, v1y, v1z, v2x, v2y, v2z;
	static double rx, ry, rz;
	static double q1, q2;
	static double z1, z2;
	v1x = gParticles[mi].vx;      //
	v1y = gParticles[mi].vy;      //  ���������� �������� ������ ��������
	v1z = gParticles[mi].vz;      //
	v2x = gParticles[mj].vx;      //
	v2y = gParticles[mj].vy;      //  ���������� �������� ������ ��������
	v2z = gParticles[mj].vz;      //
							// ������� ��������� �� ���� ����
	rx = gParticles[mi].x - gParticles[mj].x;
	ry = gParticles[mi].y[ml] - gParticles[mj].y[mk];
	rz = gParticles[mi].z[ml] - gParticles[mj].z[mk];
	z1 = rx*rx + ry*ry + rz*rz;
	// ������ ��������� ���������� ��������� ����������� ��������
	q1 = (rx*v1x + ry*v1y + rz*v1z) / z1; // 4.0; // ��������� ������ ��������
	q2 = (rx*v2x + ry*v2y + rz*v2z) / z1; // 4.0; // ��������� ������ ��������
										  // ������� ����� ��������� �������
	z1 = q2 - q1;
	gParticles[mi].vx += rx * z1;
	gParticles[mi].vy += ry * z1;
	gParticles[mi].vz += rz * z1;
	z2 = q1 - q2;
	gParticles[mj].vx += rx * z2;
	gParticles[mj].vy += ry * z2;
	gParticles[mj].vz += rz * z2;

	// ������ ���� ��������� ��� ������� ��������
	/*
	if ((ml == 0) && (mk == 0) &&
	(gParticles[mi].x > x1) && (gParticles[mj].x > x1) &&
	(gParticles[mi].x < x2) && (gParticles[mj].x < x2)) CR++;
	if (CR >= 1000*Npress) STOP = true;
	*/
}

////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////

///////////////////////////////
// ��������� �������� ������.
// ����� �������� ��������� ��� ����������� �� ������ 1 ��� �-1
// ��������� �������� � �������� ����������:
// mi - ����� ��������, � ������� ��������� �����
// mj - ����� ������, �� ������� ��������� �����
// ���������� - ��������� ��������� �������� �������� ������ �� �� ���������� � 13-14 �����.
void create(int mi, int mj) {
	switch (mj) {
	case 1:     // �������� �� ������� Y = �-1
				// �������� ���������� �������
		gParticles[mi].y[1] = -1.0; gParticles[mi].z[1] = gParticles[mi].z[0];
		// ���� � ������� ��� �������, �� ������� ���� �����, ������� �� ����������
		if (gParticles[mi].NOBR == 0) { 
			gParticles[mi].NOBR = 1; 
			gParticles[mi].OBR = 1; 
		} else { 
			// ����� � ������� ���� �����, ������� ������ ����� � ������, ������� �� ����������
			gParticles[mi].y[3] = gParticles[mi].y[1]; gParticles[mi].z[3] = gParticles[mi].z[2]; gParticles[mi].NOBR = 3;
		}
		break;
	case 3:     // �������� �� ������� Y = 1
				// �������� ���������� �������
		gParticles[mi].y[1] = A + 1.0; gParticles[mi].z[1] = gParticles[mi].z[0];
		// ���� � ������� ��� �������, �� ������� ���� �����, ������� �� ����������
		if (gParticles[mi].NOBR == 0) { gParticles[mi].NOBR = 1; gParticles[mi].OBR = 1; } else
			// ����� � ������� ���� �����, ������� ������ ����� � ������, ������� �� ����������
		{
			gParticles[mi].y[3] = gParticles[mi].y[1]; gParticles[mi].z[3] = gParticles[mi].z[2]; gParticles[mi].NOBR = 3;
		}
		break;
	case 5:     // �������� �� ������� Z = A-1
				// �������� ���������� �������
		gParticles[mi].z[2] = -1.0; gParticles[mi].y[2] = gParticles[mi].y[0];
		// ���� � ������� ��� �������, �� ������� ���� �����, ������� �� ����������
		if (gParticles[mi].NOBR == 0) { gParticles[mi].NOBR = 1; gParticles[mi].OBR = 2; } else
			// ����� � ������� ���� �����, ������� ������ ����� � ������, ������� �� ����������
		{
			gParticles[mi].y[3] = gParticles[mi].y[1]; gParticles[mi].z[3] = gParticles[mi].z[2]; gParticles[mi].NOBR = 3;
		}
		break;
	case 7:     // �������� �� ������� Z = 1
				// �������� ���������� �������
		gParticles[mi].z[2] = A + 1.0; gParticles[mi].y[2] = gParticles[mi].y[0];
		// ���� � ������� ��� �������, �� ������� ���� �����, ������� �� ����������
		if (gParticles[mi].NOBR == 0) { gParticles[mi].NOBR = 1; gParticles[mi].OBR = 2; } else
			// ����� � ������� ���� �����, ������� ������ ����� � ������, ������� �� ����������
		{
			gParticles[mi].y[3] = gParticles[mi].y[1]; gParticles[mi].z[3] = gParticles[mi].z[2]; gParticles[mi].NOBR = 3;
		}
		break;
	}
}


/////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////

void reform(int &mi, int &ml, int &mj, int &mk) {
	//static int N1;
	if (mk < 0) {
		switch (mj) {
		case 0:         // Y = A;
			gParticles[mi].y[0] = A;
			gParticles[mi].y[1] = 0.0;
			reName(mi, mj);
			break;
		case 1:         // Y = A - 1;
			gParticles[mi].y[0] = A - 1.0;
			if (gParticles[mi].vy > 0.0) create(mi, mj);
			else if (gParticles[mi].NOBR == 1) gParticles[mi].NOBR = 0;
			else { gParticles[mi].NOBR = 1; gParticles[mi].OBR = 2; }
			break;
		case 2:        // Y = 0;
			gParticles[mi].y[0] = 0.0;
			gParticles[mi].y[1] = A;
			reName(mi, mj);
			break;
		case 3:      // Y = 1;
			gParticles[mi].y[0] = 1.0;
			if (gParticles[mi].vy < 0.0) create(mi, mj);
			else if (gParticles[mi].NOBR == 1) gParticles[mi].NOBR = 0;
			else { gParticles[mi].NOBR = 1; gParticles[mi].OBR = 2; }
			break;
		case 4:    // Z = A;
			gParticles[mi].z[0] = A;
			gParticles[mi].z[2] = 0.0;
			reName(mi, mj);
			break;
		case 5:     // Z = A -1;
			gParticles[mi].z[0] = A - 1.0;
			if (gParticles[mi].vz > 0.0) create(mi, mj);
			else if (gParticles[mi].NOBR == 1) gParticles[mi].NOBR = 0;
			else { gParticles[mi].NOBR = 1; gParticles[mi].OBR = 1; }
			break;
		case 6:     // Z = 0;
			gParticles[mi].z[0] = 0.0;
			gParticles[mi].z[2] = A;
			reName(mi, mj);
			break;
		case 7:       // Z = 1;
			gParticles[mi].z[0] = 1.0;
			if (gParticles[mi].vz < 0.0) create(mi, mj);
			else if (gParticles[mi].NOBR == 1) gParticles[mi].NOBR = 0;
			else { gParticles[mi].NOBR = 1; gParticles[mi].OBR = 1; }
			break;
		case 8:        // X = 1;
			gParticles[mi].x = 1.0;
			gParticles[mi].vx *= -1.0;
			break;
		case 9:       // X = L - 3;
			gParticles[mi].x = L - 1.0;
			if (gParticles[mi].vx > 0) gParticles[mi].vx *= -1.0;
			break;
		case 10:
			gParticles[mi].x = L - 1.0;
			if (gParticles[mi].vx > 0) gParticles[mi].vx *= -1.0;
			break;
		}
	} else collision(mi, ml, mj, mk);
}

/////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////
void step(int &Nlist, int *list, int &mt, double *tm, int *im, int *lm, int *jm, int *km) {
	static double dt, ay, az;
	static int i, j, jj, mi, ml, mj, mk;
	static int N2 = N / 2;
	jj = 0;
	while ((jj < N2) && (STOP == false)) {
		dt = tm[0];
		mi = im[0]; ml = lm[0];
		mj = jm[0]; mk = km[0];

		Time = Time + dt;

		// #pragma omp parallel for private(ay,az)
		for (i = 0; i < N; i++) {
			//��������� �������� ������� �� �������� ������(� ���������)
			Del_mas(i);

			// ����������� �������
			ay = gParticles[i].vy*dt;
			gParticles[i].y[0] += ay;
			az = gParticles[i].vz*dt;
			gParticles[i].z[0] += az;
			gParticles[i].x = gParticles[i].x + gParticles[i].vx*dt;
			// ���� � ������� ��� ������� ��������� � ��������� �������
			if (gParticles[i].NOBR == 0) continue;
			else if (gParticles[i].NOBR == 1) {		// ���� � ������� ���� �����, �� ��� ���� ��������
				gParticles[i].y[1] += ay;
				gParticles[i].z[1] += az;
				gParticles[i].y[2] += ay;
				gParticles[i].z[2] += az;
			} else {	// ����� � ������� ��� ������ � �� ���� ��������.
				gParticles[i].y[1] += ay;
				gParticles[i].z[1] += az;
				gParticles[i].y[2] += ay;
				gParticles[i].z[2] += az;
				gParticles[i].y[3] += ay;
				gParticles[i].z[3] += az;
			}
		}
		// ������� ������ ������� �� ������� ������
		mt--;
		for (i = 0, j = 1; i < mt; i++, j++) {
			tm[i] = tm[j] - dt;
			im[i] = im[j]; lm[i] = lm[j];
			jm[i] = jm[j]; km[i] = km[j];
		}
		tm[mt] = 1.0E+20;
		// ��������� �������-��������� ������� � list[] � ��������� ���������
		list[Nlist] = mi;
		Nlist++;
		// ���� � ������� ����������� ��� �������, ��������� � list[] � ������ �������
		if (mk >= 0) {
			list[Nlist] = mj; 
			Nlist++; 
			jj++; 
		}
		// ���������� ��������� � ������� � ������������ � ������������ ��������
		reform(mi, ml, mj, mk);
		// ������� ������� ������ �� ���������������� �������
		purge(Nlist, list, mt, tm, im, lm, jm, km);
		/*
		// ���������.
		while ( tm[0] < 1.0E-14 ) {
		mi = im[0]; ml = lm[0];
		mj = jm[0]; mk = km[0];

		mt--;
		for (i = 0, j=1; i < mt; i++, j++) {
		tm[i] = tm[j] - dt;
		im[i] = im[j]; lm[i] = lm[j];
		jm[i] = jm[j]; km[i] = km[j];
		}

		tm[mt] = 1.0E+20;

		list[ Nlist ] = mi;
		Nlist++;

		if (mk >= 0) { list[ Nlist ] = mj; Nlist++; jj++; }

		reform(mi, ml, mj, mk);
		purge(Nlist, list, mt, tm, im, lm, jm, km);
		}
		*/
		// ����������� ����� ������� ��� ������������ � list[] ������
		retime(Nlist, list, mt, tm, im, lm, jm, km);
	}
}


//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

///  ��������� ������. ������� ������� �� ����������� ���������.
// �������� � �������� ����������:
// etta - ���������, �� ������� ���� ����� �������
// Nlist - ���������� ��������� � �������� (����� ��� �������� �����)
// list - �������� (����� ��� �������� �����)
// mt - ���������� ��������� � ������� ������
// tm, im, lm, jm, km - �������� ������� ������ (����� ��� �������� �����)
void compress(double etta, int &Nlist, int *list, int &mt, double *tm, int *im, int *lm, int *jm, int *km) {
	int MaxK, MinK, i;
	double MaxX, MinX, dx;
	if (etta > ((4.0 * PI * N) / (3.0 * A * A * (L - 2.0)))) {	// ���� ���� ��������� �����.
		while (etta > ((4.0 * PI * N) / (3.0 * A * A * (L - 1.0)))) {
			MaxK = 0;
			MinK = 0;
			MaxX = gParticles[0].x;
			MinX = gParticles[0].x;

			step(Nlist, list, mt, tm, im, lm, jm, km);

			for (i = 0; i<N; i++) {
				if (gParticles[i].x < MinX) { MinX = gParticles[i].x; MinK = i; }
				if (gParticles[i].x > MaxX) { MaxX = gParticles[i].x; MaxK = i; }
			}

			if ((MinX - 1.0) < (L - MaxX - 1.0)) dx = MinX - 1.0; else dx = L - MaxX - 1.0;

			if (gParticles[MinK].vx < 0) gParticles[MinK].vx = -gParticles[MinK].vx;
			if (gParticles[MaxK].vx > 0) gParticles[MaxK].vx = -gParticles[MaxK].vx;

			printf("%f\n", dx);
			if (dx>0) {
				L = L - dx - dx;

				for (i = 0; i<N; i++) {
					gParticles[i].x = gParticles[i].x - dx;
					gParticles[i].p = L - 1.0;
				}
			}

			purgeX(Nlist, list, mt, tm, im, lm, jm, km);
			retime(Nlist, list, mt, tm, im, lm, jm, km);
		}
	} else {	// ���� ���� ��������� �����
		L = 2.0 + (4.0 * PI * N) / (3.0 * A * A * etta);
	}
	printf("etta=%f\n", (4.0 * PI * N) / (3.0 * A * A * (L - 2.0)));
	printf("A=%f  L=%f\n", A, L);
}

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

// ��������� ��������� ��������
// ���������� �����, �� ������� � ���������� ������ ���������� Npress ����������
// ��� ����� ������������� � ������� (����������� � �������� ��� ������������ �������)
void Pressure(int &Nlist, int *list, int &mt, double *tm, int *im, int *lm, int *jm, int *km) {
	int i, j, k;

	// ����� ����������� ��������� ������� ������������ ����������
	x1 = -20.0;
	x2 = 20;
	k = 0;
	// ������������ ����� ������ � ��������� �������, ����� � ���������� �����
	// ������� ���������� �������� ��������
	for (i = 1; i < 1000; i++) {
		step(Nlist, list, mt, tm, im, lm, jm, km);
		step(Nlist, list, mt, tm, im, lm, jm, km);
		for (j = 1; j < N; j++) 
			if ((gParticles[j].x > x1) && (gParticles[j].x < x2)) 
				k++;
	}
	Npress = k / 1000.0;

	printf("\n pressure start \n");

	// ���������� ���������, ���������������� OX, �������� �������, � ������� ���������� ��������
	x1 = 20.0;
	x2 = L - 20;

	k = 0;			// ���������� ��� �������� ������ � ��������� �������
	i = 0;			// ����� ������� ��������� � ���������� �������
	CR = 0;			// ����� ���������� � ��������� �������
	Time = 0.0;		// �����, �� ������� � ��������� ������� ���������� Npress ����������
	STOP = false;	// ���������� ��� ����������� ���������� ��������� ��������
	while (STOP == false) {
		step(Nlist, list, mt, tm, im, lm, jm, km);
		i++;
		for (j = 1; j < N; j++) if ((gParticles[j].x > x1) && (gParticles[j].x < x2)) k++;
	}

	// ������� ������� ��������� � �������, ��� ������� ���������� ��������
	printf("\n\n etta liquid x>>[ %f; %f ] = %.15le", x1, x2, ((double)k / (double)i)*(4.0 / 3.0)*PI / (A*A*(x2 - x1)));
	// ������� �����
	printf("\n Time = %.15le \n", Time);
	printf("Program complete");
}

// ��������� ��������
// ��������� ������� ���� ������� � ������� ������ � �������������
// �������� � �������� ����������:
// i - ����� ����, �� ������� ���������� ��������. ��� ����������� �������� ����� �������� ������������� ��������.
// mt - ���������� ��������� � ������� ������
// tm,im,lm,jm,km - �������� ������� ������
void check(int ii, int &mt, double *tm, int *im, int *lm, int *jm, int *km) {
	static int i, j, b;
	static double r, rx, ry, rz;
	// �������� ���������� ������ ������� � ������� ������
	for (i = 0; i < N; i++) {
		b = 0;
		for (j = 0; j < mt; j++)
			if ((im[j] == i) || ((jm[j] == i) && (km[j] >= 0))) b = 1;

		if (b == 0) {
			printf("ERROR: molekula %i ne v lineike!\n", i);
			LOGFILE.printf("�� ���� %i �������� %i �� ��������� � ������� ������.\n", ii, i);
		}
	}
	// �������� �� ������������� ������
	for (i = 0; i < N - 1; i++)
		for (j = i + 1; j < N; j++) {
			rx = gParticles[i].x - gParticles[j].x;
			ry = gParticles[i].y[0] - gParticles[j].y[0];
			rz = gParticles[i].z[0] - gParticles[j].z[0];
			r = sqrt(rx*rx + ry*ry + rz*rz);
			if (r < 2.0 - 1.0E-14) {
				printf("ERROR: proniknovenie! %.5le\n", (2.0 - r));
				LOGFILE.printf("�� ���� %i ��������� ������������� �� %.15le.\n", ii, (2.0 - r));
				LOGFILE.printf("������������� ����� %i[] � %i[].\n", i, j);
			}
		}
	///////////////////////////////////////////////////////////
	for (i = 0; i < N; i++)
		if ((gParticles[i].x < 1.0) || (gParticles[i].x > L) ||
			(gParticles[i].y[0] > A) || (gParticles[i].y[0] < 0.0) ||
			(gParticles[i].z[0] > A) || (gParticles[i].z[0] < 0.0)) {
			printf("ERROR: vilet! %f\n", gParticles[i].x);
			LOGFILE.printf("�� ���� %i ��������� ����� �������� �� ������� ������.", ii);
			LOGFILE.printf("���������� �������� %i:\n", i);
			LOGFILE.printf("x=%.15le  y=%.15le  z=%.15le.\n", gParticles[i].x, gParticles[i].y[0], gParticles[i].z[0]);
		}
	////////////////////////////////////////////////////////////
	for (i = 0; i < mt - 1; i++)
		if (tm[i] > tm[i + 1]) {
			printf("ERROR: narushenie lineiki vremen!\n");
			LOGFILE.printf("��������� ������� ������. ��� %i\n", ii);
			LOGFILE.printf("%.15le  %.15le\n", tm[i], tm[i + 1]);
		}
}

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

void global_stop() {
	gStopFlag = true;
	printf("\nStop signal recieved! Saving and terminating... \n");
}

#ifdef _WIN32
	#include <windows.h>
//
	BOOL CtrlHandler(DWORD fdwCtrlType) {
		switch (fdwCtrlType) {
			// Handle the CTRL-C signal. 
			case CTRL_C_EVENT:
				global_stop();
				return(TRUE);
			default:
				return FALSE;
		}
	}
	void RegisterHandler() {
		SetConsoleCtrlHandler((PHANDLER_ROUTINE)CtrlHandler, TRUE);
	}
#else
	#include <signal.h>
	#include <unistd.h>
//
	void my_handler(int s) {
		global_stop();
	}
	void RegisterHandler() {
		struct sigaction sigIntHandler;

		sigIntHandler.sa_handler = my_handler;
		sigemptyset(&sigIntHandler.sa_mask);
		sigIntHandler.sa_flags = 0;

		sigaction(SIGINT, &sigIntHandler, NULL);
	}
#endif

bool load_snapshot(int &Nlist, int *list, int &mt, double *tm, int *im, int *lm, int *jm, int *km) {
	try {
		SNAPSHOTS.load_last();
	} catch (const char* e) {
		printf("Error: %s\n", e);
		return false;
	}
	purgeX(Nlist, list, mt, tm, im, lm, jm, km);
	retime(Nlist, list, mt, tm, im, lm, jm, km);
	return true;
}

void parse_params(int argc, char** argv) {
	int mt = 0;	// ���������� ��������� � ������� ������
	double tm[N_MAX]; // ������� �������� ������� ������.
	int im[N_MAX];
	int lm[N_MAX];
	int jm[N_MAX];
	int km[N_MAX];
	int Nlist; // ���������� ��������� � �������� � �������
	int list[N_MAX]; // �������� �������� � �������
	if (argc == 3 && strcmp(argv[1], "-compress") == 0) {
		if (!load_snapshot(Nlist, list, mt, tm, im, lm, jm, km))
			return;
		double etta = atof(argv[2]);
		printf("start compress system...\n");
		compress(etta, Nlist, list, mt, tm, im, lm, jm, km);
		printf("complite compress system\n");
	} else if (argc == 3 && strcmp(argv[1], "-generate") == 0) {
		double etta = atof(argv[2]);
		new_seed(8, etta, 0.1L);
		purgeX(Nlist, list, mt, tm, im, lm, jm, km);
		retime(Nlist, list, mt, tm, im, lm, jm, km);
		SNAPSHOTS.save_current();
	} else if (argc == 3 && strcmp(argv[1], "-step") == 0) {
		if (!load_snapshot(Nlist, list, mt, tm, im, lm, jm, km))
			return;
		int save_after_step = atoi(argv[2]);
		if (save_after_step > 0) {
			while (!gStopFlag) {
				int j = 0;
				for (; j < save_after_step && !gStopFlag; j++)
					step(Nlist, list, mt, tm, im, lm, jm, km);
				printf("Saved after %i steps\n", j);
				SNAPSHOTS.save_current();
				check(-100, mt, tm, im, lm, jm, km);
			}
		}
	} else {
		printf("examples:\n"
			   "  cube -compress [etta:float]\n"
			   "  cube -generate [etta:float]\n"
			   "  cube -step [autosave_step:int]\n"
			   );
	}
}

void load_MassArray() {
	char filename[32];
	for (int i = 0; i < sizeof(MassArray) / sizeof(MassArray[0]); i++) {
		sprintf(filename, "%d.txt", i);
		FILE* tmp_fp = fopen(filename, "r");
		if (!tmp_fp) continue;
		int it = 0;
		while (!feof(tmp_fp)) {
			fscanf(tmp_fp, "%d", &MassArray[i].Num[it]);
			MassArray[i].m[it] = true;
			it++;
		}
		MassArray[i].it = it;
		fclose(tmp_fp);
	}
}

void save_MassArray() {
	char filename[32];
	for (int i = 0; i < sizeof(MassArray) / sizeof(MassArray[0]); i++) {
		sprintf(filename, "%d.txt", i);
		FILE* tmp_fp = fopen(filename, "w+");
		if (!tmp_fp) continue;
		int it = 0;
		for (int j = 0; j < MassArray[i].it; j++)
			if (MassArray[i].m[j] == true)
				fprintf(tmp_fp, "%d\n", MassArray[i].Num[j]);
		fclose(tmp_fp);
	}
}

int main(int argc, char** argv) {
	RegisterHandler(); // Ctrl + C
	load_MassArray();
	parse_params(argc, argv);
	save_MassArray();
	SNAPSHOTS.print_catalog();
	//system("pause");
	return 0;
}
