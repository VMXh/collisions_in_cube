#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow) {
	ui->setupUi(this);
	SS = NULL;
}

MainWindow::~MainWindow() {
	delete SS;
	delete ui;
}

void MainWindow::on_listWidget_itemActivated(QListWidgetItem *) {

}

void MainWindow::on_listWidget_currentItemChanged(QListWidgetItem *current, QListWidgetItem *) {
	if (!SS || !current) return;
	QVariant data = current->data(Qt::UserRole);
	int index = data.toInt();
	int N;
	double A, L;
	particle_t* pParticles;
	SS->get_snapshot(index, N, A, L, pParticles);

	ui->label_N->setText(QString::number(N));
	ui->label_A->setText(QString::number(A));
	ui->label_L->setText(QString::number(L));
	ui->tableWidget->setRowCount(0);
	for (int i=0;i<N; i++) {
		ui->tableWidget->insertRow(i);
		ui->tableWidget->setItem(i, 0, new QTableWidgetItem(QString::number(pParticles[i].x)));
		ui->tableWidget->setItem(i, 1, new QTableWidgetItem(QString::number(pParticles[i].y[0])));
		ui->tableWidget->setItem(i, 2, new QTableWidgetItem(QString::number(pParticles[i].z[0])));
		ui->tableWidget->setItem(i, 3, new QTableWidgetItem(QString::number(pParticles[i].p)));
		ui->tableWidget->setItem(i, 4, new QTableWidgetItem(QString::number(pParticles[i].vx)));
		ui->tableWidget->setItem(i, 5, new QTableWidgetItem(QString::number(pParticles[i].vy)));
		ui->tableWidget->setItem(i, 6, new QTableWidgetItem(QString::number(pParticles[i].vz)));
	}

	delete [] pParticles;
}

void MainWindow::reloadSnapshots() {
	SnapshotSerializer::snapshot_catalog_t* catalog;
	int count = SS->get_catalog(catalog);
	ui->tableWidget->setRowCount(0);
	ui->listWidget->clear();
	for (int i = 0; i < count; i++) {
		QListWidgetItem *newItem = new QListWidgetItem;
		newItem->setData(Qt::UserRole, i);
		char timestamp_string[32];
		strftime(timestamp_string, sizeof(timestamp_string), "%Y-%m-%d %H:%M:%S", localtime(&catalog[i].timestamp));
		newItem->setText(QString::number(i) + " - " + timestamp_string);
		ui->listWidget->insertItem(i, newItem);
	}
	delete [] catalog;
	if (ui->listWidget->count() > 0)
		ui->listWidget->setCurrentItem(ui->listWidget->item(ui->listWidget->count() - 1));
}

void MainWindow::on_pushButton_3_clicked() {
	QString FilePath = QFileDialog::getOpenFileName(this, "Open BIN file", NULL, "BIN Files (*.bin)");
	if (FilePath != "") {
		if (SS) delete SS;
		try {
			SS = new SnapshotSerializer(FilePath.toStdString().c_str());
			reloadSnapshots();
		} catch(const char* error) {
			QMessageBox msgBox;
			msgBox.setText(error);
			msgBox.exec();
		}
	}
}

void MainWindow::on_pushButton_5_clicked() {
	if (SS) {
		QString FilePath = QFileDialog::getOpenFileName(this, "Open TXT file", NULL, "TXT Files (*.txt)");
		if (FilePath != "") {
			int N;
			double A, L;
			FILE* in = fopen(FilePath.toStdString().c_str(), "r");
			fscanf(in, "%d\n", &N);
			fscanf(in, "%le\n", &A);
			fscanf(in, "%le\n", &L);
			particle_t* particles = new particle_t[N];
			for (int i = 0; i < N; i++) {
				fscanf(in, "%le %le %le %le\n", &particles[i].x, &particles[i].y[0], &particles[i].z[0], &particles[i].p);
				fscanf(in, "%le %le %le\n", &particles[i].vx, &particles[i].vy, &particles[i].vz);
			}
			fclose(in);
			SS->add_snapshot(N, A, L, particles);
			delete [] particles;
			reloadSnapshots();
		}
	} else {
		QMessageBox msgBox;
		msgBox.setText("Open BIN file first!");
		msgBox.exec();
	}
}

void MainWindow::on_pushButton_4_clicked() {
	QString FilePath = QFileDialog::getSaveFileName(this, "Save BIN file", NULL, "BIN Files (*.bin)");
	if (FilePath != "") {
		QFile::remove(FilePath);
		if (SS) delete SS;
		SS = new SnapshotSerializer(FilePath.toStdString().c_str());
		reloadSnapshots();
	}
}

void MainWindow::on_pushButton_2_clicked() {
	QListWidgetItem* current = ui->listWidget->currentItem();
	if (current) {
		QString FilePath = QFileDialog::getSaveFileName(this, "Save TXT file", NULL, "TXT Files (*.txt)");
		if (FilePath != "") {
			FILE* fp = fopen(FilePath.toStdString().c_str(), "w+");
			QVariant data = current->data(Qt::UserRole);
			int index = data.toInt();
			int N;
			double A, L;
			particle_t* pParticles;
			SS->get_snapshot(index, N, A, L, pParticles);
			fprintf(fp, "%d\n", N);
			fprintf(fp, "%.15le\n", A);
			fprintf(fp, "%.15le\n", L);
			for (int i=0;i<N; i++) {
				fprintf(fp, "%.15le %.15le %.15le %.15le\n", pParticles[i].x, pParticles[i].y[0], pParticles[i].z[0], pParticles[i].p);
				fprintf(fp, "%.15le %.15le %.15le\n", pParticles[i].vx, pParticles[i].vy, pParticles[i].vz);
			}
			fclose(fp);
		}
	} else {
		QMessageBox msgBox;
		msgBox.setText("No snapshot selected!");
		msgBox.exec();
	}
}

void MainWindow::on_pushButton_clicked() {
	QListWidgetItem* current = ui->listWidget->currentItem();
	if (current) {
		QString FilePath = QFileDialog::getSaveFileName(this, "Save CSV file", NULL, "CSV Files (*.csv)");
		if (FilePath != "") {
			FILE* fp = fopen(FilePath.toStdString().c_str(), "w+");
			QVariant data = current->data(Qt::UserRole);
			int index = data.toInt();
			int N;
			double A, L;
			particle_t* pParticles;
			SS->get_snapshot(index, N, A, L, pParticles);
			fprintf(fp, "N;A;L\n");
			fprintf(fp, "%d;%.15le;%.15le\n", N, A, L);
			fprintf(fp, "x;y;z;p;vx;vy;vz\n");
			for (int i=0;i<N; i++)
				fprintf(fp, "%.15le;%.15le;%.15le;%.15le;%.15le;%.15le;%.15le\n", pParticles[i].x, pParticles[i].y[0], pParticles[i].z[0], pParticles[i].p, pParticles[i].vx, pParticles[i].vy, pParticles[i].vz);
			fclose(fp);
		}
	} else {
		QMessageBox msgBox;
		msgBox.setText("No snapshot selected!");
		msgBox.exec();
	}
}
