#-------------------------------------------------
#
# Project created by QtCreator 2016-03-15T04:50:07
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = untitled
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    snapshotserializer.cpp

HEADERS  += mainwindow.h \
    snapshotserializer.h

FORMS    += mainwindow.ui
