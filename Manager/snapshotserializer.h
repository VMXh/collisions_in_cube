#ifndef SNAPSHOTSERIALIZER_H
#define SNAPSHOTSERIALIZER_H

#include <vector>

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <stdarg.h>
#include <assert.h>
#include <time.h>
#include <string.h>

using namespace std;

// структура координат частиц.
struct particle_t {
	// координата Х для частицы и ее образов одинакова.
	// координаты Y и Z для частицы и ее оразов различны.
	double x;
	double y[4];
	double z[4];
	double vx, vy, vz;

	int NOBR; // NOBR - количество образов, которое имеет частица.
	int OBR; // OBR - номер образа, если он единственный (1 - по Y, 2 - по Z)
	double p; // p - координата, где должна отразиться частица на границе, противоположной идеальной стенке.
};

class SnapshotSerializer {
private:
	typedef __int64 off64_t;

#pragma pack(push, 1)
	typedef struct {
		double x;
		double y;
		double z;
		double p;
		double vx;
		double vy;
		double vz;
	} compact_particle_t;
	typedef struct {
		int N;
		double A;
		double L;
	} element_hdr_t;
#pragma pack(pop)

	static const uint8_t EAT_flag_VALID = 1 << 0;

#pragma pack(push, 1)
	typedef struct {
		uint8_t flags; // EAT_flag_*
		uint64_t timestamp;
		off64_t start_offset;
		uint32_t size;
	} EAT_element_t;
#pragma pack(pop)

	// EAT = Element Allocation Table
	static const int EAT_max_elements_count = 5; // максимальное количество Element адресуемых EAT плюс 1 запись зарезервированая для указателя на следующую EAT
	static const off64_t EAT_size = sizeof(EAT_element_t)*EAT_max_elements_count;
	static const off64_t EAT_last_element_offset = sizeof(EAT_element_t)*(EAT_max_elements_count - 1);
	//
	static uint8_t empty_EAT[EAT_size];
	//
	FILE* fp;
	//
	size_t EAT_free_element_index;
	off64_t free_element_offset;
	//
	vector<off64_t> EAT_list;
	//
#define CURRENT_EAT_BASE EAT_list.back()
	//
	void EAT_get_element(off64_t base, int idx, time_t* timestamp, off64_t* start_offset, uint32_t* size);
	void EAT_set_element(off64_t base, int idx, off64_t start_offset, uint32_t size);
	void EAT_allocate_next_table();
	void EAT_check_overflow();
	void EAT_init_information();
public:
	typedef struct {
		off64_t base;
		uint32_t size;
		time_t timestamp;
	} snapshot_catalog_t;
	SnapshotSerializer(const char* fileName);
	void add_snapshot(int N, double A, double L, particle_t* particles);
	int get_snapshot_count();
	int get_catalog(snapshot_catalog_t*& catalog);
	void get_snapshot(int snapshot_idx, int &N, double &A, double &L, particle_t* &particles);
	void get_last_snapshot(int &N, double &A, double &L, particle_t* &particles);
	~SnapshotSerializer();
};

#endif // SNAPSHOTSERIALIZER_H
