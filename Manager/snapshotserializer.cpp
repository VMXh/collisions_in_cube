#include "snapshotserializer.h"

uint8_t SnapshotSerializer::empty_EAT[SnapshotSerializer::EAT_size];

void SnapshotSerializer::EAT_get_element(off64_t base, int idx, time_t* timestamp, off64_t* start_offset, uint32_t* size) {
	EAT_element_t EAT_element;
	off64_t saved_pos = _ftelli64(fp);
	int result;
	result = _fseeki64(fp, base + idx*sizeof(EAT_element_t), SEEK_SET);
	assert(result == 0);
	int readed = fread(&EAT_element, 1, sizeof(EAT_element), fp);
	assert(readed == sizeof(EAT_element));
	result = _fseeki64(fp, saved_pos, SEEK_SET);
	assert(result == 0);
	assert((EAT_element.flags & EAT_flag_VALID) == EAT_flag_VALID);
	if (start_offset) *start_offset = EAT_element.start_offset;
	if (size) *size = EAT_element.size;
	if (timestamp) *timestamp = (time_t)EAT_element.timestamp;
}

void SnapshotSerializer::EAT_set_element(off64_t base, int idx, off64_t start_offset, uint32_t size) {
	time_t timestamp;
	EAT_element_t EAT_element;
	int result;
	//
	time(&timestamp);
	memset(&EAT_element, 0, sizeof(EAT_element));
	//
	off64_t saved_pos = _ftelli64(fp);
	result = _fseeki64(fp, base + idx*sizeof(EAT_element_t), SEEK_SET);
	assert(result == 0);
	EAT_element.flags = EAT_flag_VALID;
	EAT_element.start_offset = start_offset;
	EAT_element.size = size;
	EAT_element.timestamp = (uint64_t)timestamp;
	int writted = fwrite(&EAT_element, 1, sizeof(EAT_element), fp);
	assert(writted == sizeof(EAT_element));
	result = _fseeki64(fp, saved_pos, SEEK_SET);
	assert(result == 0);
}

void SnapshotSerializer::EAT_allocate_next_table() {
	EAT_set_element(CURRENT_EAT_BASE, EAT_max_elements_count - 1, free_element_offset, EAT_size);
	EAT_list.push_back(free_element_offset);
	EAT_free_element_index = 0;
	//
	int writted = fwrite(empty_EAT, 1, EAT_size, fp);
	assert(writted == EAT_size);
	free_element_offset = CURRENT_EAT_BASE + EAT_size;
	//
	assert(_ftelli64(fp) == free_element_offset);
}

void SnapshotSerializer::EAT_check_overflow() {
	if (EAT_free_element_index == EAT_max_elements_count - 1)
		EAT_allocate_next_table();
}

void SnapshotSerializer::EAT_init_information() {
	size_t readed;
	int result;
	off64_t filesize;
	//
	result = _fseeki64(fp, 0, SEEK_END);
	assert(result == 0);
	filesize = _ftelli64(fp);
	if (filesize == 0)
		fwrite(empty_EAT, 1, EAT_size, fp);
	//
	EAT_list.push_back(0);
	for (;;) { // locate last EAT in file
		result = _fseeki64(fp, CURRENT_EAT_BASE + EAT_last_element_offset, SEEK_SET);
		assert(result == 0);
		EAT_element_t EAT_element;
		readed = fread(&EAT_element, 1, sizeof(EAT_element), fp);
		if (readed != sizeof(EAT_element)) throw "StateSerializer: EAT element is truncated!";
		if ((EAT_element.flags & EAT_flag_VALID) != EAT_flag_VALID) break; // данная EAT последняя, тк последний элемент (указатель на следующую таблицу) не инициализирован
		EAT_list.push_back(EAT_element.start_offset);
	}
	result = _fseeki64(fp, CURRENT_EAT_BASE, SEEK_SET);
	assert(result == 0);
	EAT_element_t* pEAT = new EAT_element_t[EAT_max_elements_count];
	readed = fread(pEAT, 1, sizeof(*pEAT) * EAT_max_elements_count, fp);
	EAT_free_element_index = EAT_max_elements_count - 1;
	free_element_offset = CURRENT_EAT_BASE + EAT_size;
	for (int i = 0; i < EAT_max_elements_count - 1; i++) { // ищем последний неинициализированный элемент EAT
		if ((pEAT[i].flags & EAT_flag_VALID) != EAT_flag_VALID) {
			EAT_free_element_index = i;
			break;
		}
		free_element_offset = pEAT[i].start_offset + pEAT[i].size;
	}
	result = _fseeki64(fp, free_element_offset, SEEK_SET);
	assert(result == 0);
}

SnapshotSerializer::SnapshotSerializer(const char* fileName) {
	memset(empty_EAT, 0, EAT_size);
	if (fopen_s(&fp, fileName, "rb+") != 0 && fopen_s(&fp, fileName, "wb+") != 0)
		throw "can't open snapshot file";
	EAT_init_information();
}

void SnapshotSerializer::add_snapshot(int N, double A, double L, particle_t* particles) {
	EAT_check_overflow();
	//
	uint32_t element_size = sizeof(element_hdr_t) + sizeof(compact_particle_t) * N;
	//
	uint8_t* pElement = new uint8_t[element_size];
	element_hdr_t* element_hdr = (element_hdr_t*)pElement;
	element_hdr->N = N;
	element_hdr->A = A;
	element_hdr->L = L;
	compact_particle_t* pParticles = (compact_particle_t*)(pElement + sizeof(element_hdr_t));
	for (int i = 0; i < N; i++) {
		pParticles[i].x = particles[i].x;
		pParticles[i].y = particles[i].y[0];
		pParticles[i].z = particles[i].z[0];
		pParticles[i].p = particles[i].p;
		pParticles[i].vx = particles[i].vx;
		pParticles[i].vy = particles[i].vy;
		pParticles[i].vz = particles[i].vz;
	}
	size_t writted = fwrite(pElement, 1, element_size, fp);
	assert(writted == element_size);
	fflush(fp);
	EAT_set_element(CURRENT_EAT_BASE, EAT_free_element_index, free_element_offset, element_size);
	free_element_offset += element_size;
	EAT_free_element_index++;
	delete[] pElement;
	//
	assert(_ftelli64(fp) == free_element_offset);
}

int SnapshotSerializer::get_snapshot_count() {
	return (EAT_list.size() - 1) * (EAT_max_elements_count - 1) + EAT_free_element_index;
}

int SnapshotSerializer::get_catalog(snapshot_catalog_t*& catalog) {
	int snapshot_count = get_snapshot_count();
	catalog = new snapshot_catalog_t[snapshot_count + 1];
	int catalog_pos = 0;
	int result;
	EAT_element_t* pEAT = new EAT_element_t[EAT_max_elements_count];
	for (size_t i = 0; i < EAT_list.size(); i++) {
		off64_t saved_pos = _ftelli64(fp);
		result = _fseeki64(fp, EAT_list[i], SEEK_SET);
		assert(result == 0);
		int readed = fread(pEAT, 1, EAT_size, fp);
		assert(readed == EAT_size);
		result = _fseeki64(fp, saved_pos, SEEK_SET);
		assert(result == 0);
		for (int j = 0; j < EAT_max_elements_count - 1; j++) {
			if ((pEAT[j].flags & EAT_flag_VALID) != EAT_flag_VALID)
				break;
			catalog[catalog_pos].base = pEAT[j].start_offset;
			catalog[catalog_pos].size = pEAT[j].size;
			catalog[catalog_pos].timestamp = (time_t)pEAT[j].timestamp;
			catalog_pos++;
		}
	}
	return snapshot_count;
}

void SnapshotSerializer::get_snapshot(int snapshot_idx, int &N, double &A, double &L, particle_t* &particles) {
	size_t target_EAT_index = snapshot_idx / (EAT_max_elements_count - 1);
	size_t target_EAT_element_index = snapshot_idx % (EAT_max_elements_count - 1);
	//
	assert(target_EAT_index < EAT_list.size());
	assert(!(target_EAT_index == EAT_list.size() && target_EAT_element_index >= EAT_free_element_index));
	off64_t target_EAT_base = EAT_list[target_EAT_index];
	//
	off64_t element_base;
	uint32_t element_size;
	EAT_get_element(target_EAT_base, target_EAT_element_index, NULL, &element_base, &element_size);
	//
	uint8_t* pElement = new uint8_t[element_size];
	//
	off64_t saved_pos = _ftelli64(fp);
	int result = _fseeki64(fp, element_base, SEEK_SET);
	assert(result == 0);
	int readed = fread(pElement, 1, element_size, fp);
	assert(readed == element_size);
	result = _fseeki64(fp, saved_pos, SEEK_SET);
	assert(result == 0);
	//
	element_hdr_t* element_hdr = (element_hdr_t*)pElement;
	N = element_hdr->N;
	A = element_hdr->A;
	L = element_hdr->L;
	particles = new particle_t[N];
	//
	compact_particle_t* pParticles = (compact_particle_t*)(pElement + sizeof(element_hdr_t));
	for (int i = 0; i < N; i++) {
		particles[i].x = pParticles[i].x;
		particles[i].y[0] = pParticles[i].y;
		particles[i].z[0] = pParticles[i].z;
		particles[i].p = pParticles[i].p;
		particles[i].vx = pParticles[i].vx;
		particles[i].vy = pParticles[i].vy;
		particles[i].vz = pParticles[i].vz;
	}
	delete[] pElement;
}

void SnapshotSerializer::get_last_snapshot(int &N, double &A, double &L, particle_t* &particles) {
	assert(get_snapshot_count() > 0);
	get_snapshot(get_snapshot_count() - 1, N, A, L, particles);
}

SnapshotSerializer::~SnapshotSerializer() {
	fclose(fp);
}
