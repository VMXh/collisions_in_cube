#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QListWidget>
#include <QFileDialog>
#include <QMessageBox>

#include "snapshotserializer.h"

namespace Ui {
	class MainWindow;
}

class MainWindow : public QMainWindow
{
	Q_OBJECT

public:
	explicit MainWindow(QWidget *parent = 0);
	~MainWindow();

private slots:
	void on_listWidget_itemActivated(QListWidgetItem *item);

	void on_listWidget_currentItemChanged(QListWidgetItem *current, QListWidgetItem *previous);

	void on_pushButton_3_clicked();

	void on_pushButton_5_clicked();

	void on_pushButton_4_clicked();

	void on_pushButton_2_clicked();

	void on_pushButton_clicked();

private:
	void reloadSnapshots();

	Ui::MainWindow *ui;
	SnapshotSerializer* SS;
};

#endif // MAINWINDOW_H
